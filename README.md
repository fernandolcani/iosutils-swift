# iOSUtils-Swift

[![CI Status](http://img.shields.io/travis/Gregory Sholl e Santos/iOSUtils-Swift.svg?style=flat)](https://travis-ci.org/Gregory Sholl e Santos/iOSUtils-Swift)
[![Version](https://img.shields.io/cocoapods/v/iOSUtils-Swift.svg?style=flat)](http://cocoapods.org/pods/iOSUtils-Swift)
[![License](https://img.shields.io/cocoapods/l/iOSUtils-Swift.svg?style=flat)](http://cocoapods.org/pods/iOSUtils-Swift)
[![Platform](https://img.shields.io/cocoapods/p/iOSUtils-Swift.svg?style=flat)](http://cocoapods.org/pods/iOSUtils-Swift)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

iOSUtils-Swift is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
For Xcode 9
pod 'iOSUtils-Swift', :git => "https://bitbucket.org/livetouchdev/iosutils-swift"

For Xcode 8
pod 'iOSUtils-Swift', :git => "https://bitbucket.org/livetouchdev/iosutils-swift", :branch => 'swift3'
```

## Author

Livetouch

## License

iOSUtils-Swift is available under the MIT license. See the LICENSE file for more info.
