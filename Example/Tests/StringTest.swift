//
//  StringTest.swift
//  iOSUtils-Swift
//
//  Created by Ricardo Lecheta on 20/10/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import iOSUtils_Swift
import XCTest

class StringTest : BaseTest {
    
    func testTrim() {
        let s = "  Teste  "

        print("[\(s)]")
        print("[\(s.trim())]")
        print("[\(StringUtils.trim(s))]")

        XCTAssertEqual("Teste", s.trim())
        XCTAssertEqual("Teste", StringUtils.trim(s))
    }
    
    func testUpperLower() {
        let s = "TeSte"

        XCTAssertEqual("TESTE", StringUtils.toUpperCase(s))
        XCTAssertEqual("teste", StringUtils.toLowerCase(s))
    }
}
