import UIKit
import XCTest
import iOSUtils_Swift
import SwiftDate

class DateTests: BaseTest {
    
    func test1() {
        
        let dt = Date()
        print(dt.toString("dd/MM/YYYY HH:mm:ss"))
        
        print(DateUtils.toString(dt,withPattern: "dd/MM/YYYY HH:mm:ss"))
    }
    
    func test2() {
        // 1476361944000
        // 13/10/2016 09:32:24
        
//        let time = 1476361944000 / 1000
//        
//        let dt = Date(timeIntervalSince1970: Double(time))
//        print(dt)
//
//        let horario = DateUtils.toString(dt, withPattern: "HH:mm")
//        print("test 2 horario \(horario)")
    }
    
    func test3() {
        // 1476361944000
        // 12:32:24
        // 09:32:24
        
        //Pega diferença em segundos de GMT para o timezone local
        //let secondsFromGMT: Int64 = NSTimeZone.local.secondsFromGMT()
        
//        let timeSeconds = 1476361944000 / 1000
//        //timeSeconds += secondsFromGMT
//        
//        let time : TimeInterval = TimeInterval(timeSeconds)
//        let dt : Date = Date(timeIntervalSince1970: time)
//        
//        print(dt)
//        
//        let dateformatter = DateFormatter()
//        
//        dateformatter.dateFormat = "HH:mm:ss"
//        
//        let horario = dateformatter.string(from: dt)
//        //let horario = DateUtils.toString(dt, withPattern: "HH:mm")
//        print("test 3 horario: \(horario)")
    }

}
