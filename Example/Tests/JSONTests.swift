import UIKit
import XCTest
import iOSUtils_Swift

//https://github.com/Hearst-DD/ObjectMapper
class JSONTests: BaseTest {
    
    func testAppleNotif() {

        let params = [
            "title": "Hello",
            "body" : "Hey Jo"
        ]
        
        let a = AppleNotification(params)
        
        a.title = "Title here"
        a.body = "Body here"
        
        print("Vai imprimir o toString")
        print(a)
    }
    
    func testPushNotif() {
        
        let params = [
            "title": "Hello",
            "body" : "Hey Jo"
        ]
        
        let a = PushNotification(params)
        
        a.collapseKey = "collapseKey here"
        a.from = "from here"
        
        print("Vai imprimir o toString")
        print(a)
    }
    
    func test1() {
        
        let p = TestPessoa()
        p.nome = "Ricardo";
        p.nome2 = "Ricardo2"
        p.idade = 35
        p.casado = true
        p.salario = 2500.45
        
        let json = p.toJSONString(prettyPrint: true)!
        print(json)
        
        print("---")
        
        let p2 = TestPessoa(JSONString: json)!
        print(p2)
    }
}
