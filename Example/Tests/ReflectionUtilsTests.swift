import UIKit
import XCTest
import iOSUtils_Swift

class ReflectionUtilsTests: BaseTest {

    func test1() {
        let fields = ReflectionUtils.getFields(TestPessoa.self)
        print(fields)
        
        print(SQLUtils.getSQLCreateTable(TestPessoa.self))
    }
    
    func test2() {

        let p = TestPessoa()
        p.nome = "Ricardo";
        p.nome2 = "Ricardo2"
        p.idade = 35
        p.casado = true
        p.salario = 2500.0
    

        let f = ReflectionUtils.getField(TestPessoa.self, fieldName: "casado")
        print(f.isType(Bool.self))

        //        ReflectionUtils.setFieldValue(obj: p,fieldName: "nome", value: "Ricardo Lecheta")
        //        ReflectionUtils.setFieldValue(obj: p,fieldName: "nome2", value: "Ricardo R Lecheta")
        ReflectionUtils.setFieldValue(obj: p,fieldName: "idade", value: 45)
        ReflectionUtils.setFieldValue(obj: p,fieldName: "casado", value: true)
        ReflectionUtils.setFieldValue(obj: p,fieldName: "salario", value: 12345.67)
        
//                var clsName = ClassUtils.getClassName(cls: cls)
//                print(clsName)
        //
        //        clsName = ClassUtils.getClassName(obj: p)
        //        print(clsName)
        //
                let fields = ReflectionUtils.getFields(TestPessoa.self)
                for f in fields {
                    if(f.name.equalsIgnoreCase(string: "other")) {
                        print("boom")
                    }
                    print("Field \(f.name) > \(f.type) > \(f.isType(TestPessoa.self))")
                }
        
        print(ReflectionUtils.getFieldValue(obj: p, fieldName: "nome"))
        print(ReflectionUtils.getFieldValue(obj: p, fieldName: "nome2"))
        
        print(p.value(forKey: "nome"))
        
        let idade = ReflectionUtils.getFieldValue(obj: p, fieldName: "idade") as! Int;
        print("idade \(idade)")
        
        let casado = ReflectionUtils.getFieldValue(obj: p, fieldName: "casado") as! Bool;
        print("casado \(casado)")
        
        let salario = ReflectionUtils.getFieldValue(obj: p, fieldName: "salario") as! Double;
        print("salario \(salario)")

    }
    
    func testTipo() {
        
        let p = TestPessoa()
        p.id = 1
        p.nome = "Ricardo";
        p.nome2 = "Ricardo2"
        p.idade = 35
        p.casado = true
        p.salario = 2500.0
        
        var s = p.value(forKey: "nome")!;
        print("Valor do nome: \(s)" )
        print("Tipo \(type(of:s)) > \(type(of:s) == String.self)")
        
        s = ReflectionUtils.getFieldValue(obj: p, fieldName: "nome")!
        print("Valor do nome: \(s)" )
        print("Tipo \(type(of:s)) > \(type(of:s) == String.self)")

        
        print("-----")
        print("Testando tipo da Pessoa")
        print("ID da pessoa é \(type(of:p.id)) > \(type(of:p.id) == Int.self)")
        print("Nome da pessoa é \(type(of:p.nome)) > \(type(of:p.nome) == String.self)")
        print("Idade da pessoa é \(type(of:p.idade)) > \(type(of:p.idade) == Int.self)")
        print("Casado da pessoa é \(type(of:p.casado)) > \(type(of:p.casado) == Bool.self)")
        print("Salario da pessoa é \(type(of:p.salario)) > \(type(of:p.salario) == Double.self)")
        print("-----")
        
        var params : [Any] = []
        
        let fields = ReflectionUtils.getFields(TestPessoa.self)
        print(fields)
        
        print("Lendo campos...")
        
        // params
        for f in fields {
            
            let value = ReflectionUtils.getFieldValue(obj: p, fieldName: f.name)!
//            let value = p.value(forKey: f.name);
            params.append(value)
            
            print("Field: \(f.name), value: \(value), tipo \(type(of:value))")
        }
        
//        print(params)
        
        print("-----")
        print("Iniciando teste de tipos")
        
        for param in params {
            
            let type = type(of:param)
            
            print("Testando param \(param), tipo: \(type) " )
            
            if(type == Int.self) {
                print("   >> Int!")
            } else if(type == String.self || type == Optional<String>.self) {
                print("   >> String!")
            } else if(type == Bool.self) {
                print("   >> Bool!")
            } else if(type == Double.self) {
                print("   >> Double!")
            } else {
                print("   >> Tipo nao encontrado.")
            }
        }
        
//        let type = type(of:"Nico")
//        print(type == String.self)
    }
}
