//
//  HelloLibDatabaseHelper.swift
//  HelloLibIOS
//
//  Created by Livetouch on 05/07/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class TestDatabaseHelper: DatabaseHelper {

    override func getDatabaseFile() throws -> String {
        return "iosutils-test.sqlite"
    }
    
    override func getDatabaseVersion() throws -> Int {
        return 5
    }
    
    override func getDomainClasses() throws -> [AnyClass] {
        return [TestPessoa.self]
    }
    
    override func onCreate() {
        SQLUtils.createDatabase()
    }
    
    override func onUpgrade(_ oldVersion: Int, newVersion: Int) {
        print("onUpgrade")
    }
    
    override func isAutoUpdate() throws -> Bool {
        return false
    }
}
