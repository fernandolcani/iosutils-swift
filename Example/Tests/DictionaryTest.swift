//
//  DictionaryTest.swift
//  iOSUtils-Swift
//
//  Created by Ricardo Lecheta on 21/10/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
//DictionaryTest

import UIKit
import XCTest
import iOSUtils_Swift
import SwiftDate

class DictionaryTest: BaseTest {
    
    func test1() {
        
        let string = "{\"r\": 48,\"g\": 63,\"b\": 159,\"a\": 1}"
        
        let dict = try! JSON.toDictionary(string)
        
        print(dict["r"]!)
        print(dict["g"]!)
        print(dict["b"]!)
        print(dict["a"]!)
        
        print(dict.getIntWithKey("r"))
    }
    
    func test2() {
        
        let dict = ["r":1,"g":2,"b":3,"a":1]
        
        print(dict["r"]!)
        print(dict["g"]!)
        print(dict["b"]!)
        print(dict["a"]!)
        
        print(dict.getIntWithKey("r"))
    }
    
}
