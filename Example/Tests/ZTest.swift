//
//  ZTest.swift
//  iOSUtils-Swift
//
//  Created by Ricardo Lecheta on 15/10/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation

class ZTest : BaseTest {
    
    func soma(n1:Int, n2:Int) -> Int {
        return n1 + n2
    }

    func test() {
        let n = soma(n1: 1, n2: 3)
        print(n)
    }
}
