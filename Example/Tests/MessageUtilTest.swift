//
//  DictionaryTest.swift
//  iOSUtils-Swift
//
//  Created by Ricardo Lecheta on 21/10/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation

import UIKit
import XCTest
import iOSUtils_Swift
import SwiftDate

class MessageUtilTest: BaseTest {
    
    func test1() {
        
        let dict = MessageUtils.getDict(fileName: "ios_utils_default_messages.json")

        let s = dict.getStringWithKey("progress_dialog_default_message")
        
        XCTAssertEqual("Aguarde..." ,s)
    }
    
}
