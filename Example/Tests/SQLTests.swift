//
//  SQLUtilsTests.swift
//  iOSUtils-Swift
//
//  Created by Ricardo Lecheta on 10/11/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import iOSUtils_Swift

class SQLTests : BaseTest {
    
    override func setUp() {
        
        DatabaseHelper.LOG_ON = true
        // DatabaseHelper.LOG_BIND_ON = true
        
        super.setUp()
    }
 
    func testAreaHF() {
        print(SQLUtils.getSQLCreateTable(TestPessoa.self))
        
    }
    
    func testSQL() {
        SQLUtils.printTable(TestPessoa.self)
    }
    
    func testSimpleEntity() {
        
        let s = TestSimpleEntity()
        print(s)
        
        s.id = 10
        print(s)
    }
    
    func testCount() {
        
//        let sql = "select * from " + SQLUtils.toSQLTableName(TestPessoa.self);
        
        var count = SQLUtils.count(TestPessoa.self)
        print(count)
        
        count = SQLUtils.count(TestPessoa.self,query: "nome=?",args:["Ricardo"])
        print(count)
    }
    
    func testGetValueOptional() {
        let p = TestPessoa()
        p.nome = "Ricardo";
        p.nome2 = "Lecheta"
        
        let value = ReflectionUtils.getFieldValue(obj: p, fieldName: "nome2")
        print(value)
        print(type(of:value))
        
        let text = value as! String
        print(text)
    }

    func testSaveInTx() {
        
        _ = TestPessoa.deleteAll()

        let p = TestPessoa()
        p.nome = "Pessoa A";
        p.nome2 = "Teste"
        p.idade = 15
        p.casado = true
        p.salario = 100
        
        let p2 = TestPessoa()
        p2.nome = "Pessoa B";
        p2.nome2 = "Sobrenome aqui"
        p2.idade = 22
        p2.casado = false
        p2.salario = 200
        
        SQLUtils.saveInTx(TestPessoa.self, list: [p,p2])
        
        let pessoas = TestPessoa.findAll() as [TestPessoa]
        print("Encontrado \(pessoas.count) pessoas")
 
        for p in pessoas {
            print("id \(p.id), nome \(p.nome), idade \(p.idade), casado \(p.casado)")
            print(p)
        }
    }

    func testPessoa() {

        _ = SQLUtils.execSql(SQLUtils.getSQLDropTable(TestPessoa.self))

        _ = SQLUtils.execSql(SQLUtils.getSQLCreateTable(TestPessoa.self))

        let p = TestPessoa()
        p.id = 5
        p.nome = "Ricardo";
        p.nome2 = "Lecheta Update2"
        p.idade = 35
        p.casado = true
        p.salario = 1000.21

        let id = p.save()
        print("Pessoa saved \(id)")

        let pessoas = TestPessoa.findAll() as [TestPessoa]
        print("Encontrado \(pessoas.count) pessoas")
        
//        pessoas = TestPessoa.findAll("id=?", args: [1])
    
        for p in pessoas {
            print("id \(p.id), nome \(p.nome), idade \(p.idade), casado \(p.casado)")
            print(p)
        }
    }
    
}
