//
//  TestEntity.swift
//  iOSUtils-Swift
//
//  Created by Ricardo Lecheta on 10/11/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import iOSUtils_Swift
import ObjectMapper

class TestPessoa : JSONEntity {

    var nome = ""
    var nome2 : String?
    
    var idade = 0
    var casado = false
    var salario = 0.0
    
    var campoParaIgnorar = "IGNORE ISSO"

    // Mappable
    override func mapping(map: Map) {
        nome      <- map["nome"]
        nome2     <- map["nome2"]
        idade     <- map["idade"]
        casado    <- map["casado"]
        salario   <- map["salario"]
    }
    
    override func entityMapping(sql: EntityMapping) {
        sql.table("PESSOA_XXX")
        sql.column("nome2", toColumn: "sobrenome")
        sql.ignore("campoParaIgnorar")
    }
}
