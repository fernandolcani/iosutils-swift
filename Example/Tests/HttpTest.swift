//
//  SQLUtilsTests.swift
//  iOSUtils-Swift
//
//  Created by Ricardo Lecheta on 10/11/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import iOSUtils_Swift

class HttpTest : BaseTest {

    override func setUp() {
        super.setUp()

        HttpHelper.LOG_ALL_ON = true
    }

    func testGetOk() throws {

        let http = HttpHelper()
        
        let params = [
            "mode"      : "json",
            "form_name" : "form",
            "wsVersion" : "3",
            "device.so" : "iOS"
        ]

        try http.get("http://livecom.livetouchdev.com.br/time.htm", withParameters: params)
        let string = http.getString()
        print(string)
    }
    
    func testGetPaginaNaoExiste() throws {
        
        let http = HttpHelper()
        
        let params = [
            "mode"      : "json",
            "form_name" : "form",
            "wsVersion" : "3",
            "device.so" : "iOS"
        ]
        
        try http.get("http://error.com/error", withParameters: params)
        let string = http.getString()
        print(string)
    }

    func testError() throws {

        let http = HttpHelper()
        
        let params = [
            "mode"      : "json",
            "form_name" : "form",
            "wsVersion" : "3",
            "device.so" : "iOS"
        ]

        do {
            try http.get("xxx://error.com/error", withParameters: params)
            let string = http.getString()
            print(string)
        } catch (let error) {
            print("OK deu erro \(error)")
        }
    }
}
