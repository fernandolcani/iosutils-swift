//
//  BaseTest.swift
//  iOSUtils-Swift
//
//  Created by Ricardo Lecheta on 10/11/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit
import XCTest
import Foundation
import iOSUtils_Swift

/**
 https://developer.apple.com/library/content/documentation/DeveloperTools/Conceptual/testing_with_xcode/chapters/05-running_tests.html
 **/
class BaseTest: XCTestCase {
    
    override func setUp() {
        print("----------------------------------------------")
        
        let dbHelper = TestDatabaseHelper()
        DatabaseHelper.instance = dbHelper
    }
    
    override func tearDown() {
        print("----------------------------------------------")
    }
}
