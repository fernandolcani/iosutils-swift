//
//  SQLUtils.swift
//  SugarIOS
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit
import sqlite3

open class SQLUtils<T:Entity> : NSObject {
    
    // Mark: Mapping

    static public func getSqlMapping(_ cls: AnyClass) -> EntityMapping {
        var map = getDatabaseHelper().mapEntityMapping

        let key = ClassUtils.getClassName(cls: cls)

        if let mapping = map[key] {
            // retorna do cache
            return mapping
        }

        // faz o mapping
        let entity = newEntity(cls)
        let sql = EntityMapping(cls: cls)
        entity.entityMapping(sql: sql);

        // salva no cache
        map[key] = sql
        getDatabaseHelper().mapEntityMapping = map
        
        return sql
    }
    
    // MARK: Init
    
    static open func getDatabaseHelper() -> DatabaseHelper {
        if let dbHelper = DatabaseHelper.instance {
            return dbHelper
        }
        
        do {
            if let baseAppDelegate = UIApplication.shared.delegate as? BaseAppDelegate {
                var dbHelper = try! baseAppDelegate.getConfig().getDatabaseHelper()
                
                DatabaseHelper.instance = dbHelper
                
                return dbHelper
            }
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
        }
        
        print("\n\n***************************")
        print("ERROR: Implemente o AppDelegate.getConfig().getDatabaseHelper()")
        print("\n\n***************************")
        
        return DatabaseHelper()
        
//        throw Exception.notImplemented
    }
    
    static open func getDb() -> OpaquePointer {
        let db = getDatabaseHelper().getDb()
        return db
    }
    
    // MARK: - CREATE & DROP
    
    static open func createDatabase() {
        do {
            let sqls = try getSQLCreateDatabase()
            for sql in sqls {
                _ = try execSql(sql)
            }
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
        }
    }
    
    static open func dropDatabase() {
        do {
            let sqls = try getSQLDropDatabase()
            for sql in sqls {
                _ = try execSql(sql)
            }
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
        }
    }
    
    static open func getSQLDropTable(_ cls: AnyClass) -> String {
        let table = SQLUtils.toSQLTableName(cls);
        let drop = "drop table if exists \(table);"
        return drop
    }
    
    static open func getSQLCreateDatabase() throws -> [String] {
        
        let classes = try getDatabaseHelper().getDomainClasses()
        
        var sql : [String] = []
        
        for cls in classes {
            let sqlCreate = getSQLCreateTable(cls)
            sql.append(sqlCreate)
        }
        
        return sql
    }
    
    static open func getSQLDropDatabase() throws -> [String] {
        
        let classes = try getDatabaseHelper().getDomainClasses()
        
        var sql : [String] = []
        
        for c in classes {
            sql.append(getSQLDropTable(c))
        }
        
        return sql
    }
    
    static open func getSQLCreateTable(_ cls: AnyClass) -> String {
        
        let fields = ReflectionUtils.getFields(cls)
        
        let table = SQLUtils.toSQLTableName(cls)
        
        var sql = "create table if not exists \(table) (id integer primary key autoincrement"
        
        var count = 0

        for f in fields {
            count+=1
            //Ignora o id, pois cria automaticamente (id integer primary key autoincrement)
            if("id".equalsIgnoreCase(string: f.name)) {
                continue
            }
            let fieldName = toSQLColumnName(cls, fieldName: f.name)
            sql.append("\(", ")\(fieldName) \(getSqlType(f))")
        }
        
        sql.append(");")
        
        return sql
    }
    
    static open func getSqlDeleteAllTables() throws -> [String] {
        let classes = try getDatabaseHelper().getDomainClasses()
        
        var sqls : [String] = []
        
        for c in classes {
            let table = SQLUtils.toSQLTableName(c)
            let sql = "delete from \(table);"
            sqls.append(sql)
        }
        
        return sqls
    }

    //MARK: - Exec SQL
    
    open static func execScript(_ filename: String) {
        var script : String?
        
        do {
            let path = try FileUtils.getResourcePathOfFile(filename)
            script = try FileUtils.readBundleFileAtPath(path)
            
        } catch {
            switch error {
            case Exception.genericException:
                log("Erro ao ler o arquivo '\(filename)'.")
                
            case Exception.fileNotFoundException:
                log("Arquivo '\(filename)' não existe.")
                
            default:
                break
            }
        }
        
        if let script = script {
            execSqlWithTx(script)
        }
        
    }
    
    
    // Executa o SQL e retorna o statement
    static func getStmt(_ sql:String, args: [Any] = []) -> OpaquePointer? {
        
        var stmt : OpaquePointer? = nil
        
        let cSql = sql.UTF8String
        
        let db = getDatabaseHelper().getDb()
        
        let result = sqlite3_prepare_v2(db, cSql, -1, &stmt, nil)
        
        if (result == SQLITE_OK) {
            
            if let stmt = stmt {
                bindargs(stmt, args:args)
                
                return stmt
            }
            
        } else {
            sqlite3_finalize(stmt)
        }
        
        logError("\n\n*********** SQL ERROR: *********** ")
        logError("Verifique a sintaxe do SQL.")
        logError(sql)
        logError("\(SQLUtils.getLastSqlError())")
        logError("*********************\n")

        return stmt
    }
    
    static func getLastSqlError() -> String {
        var err:UnsafePointer<Int8>? = nil
        
        let db = getDb()
        
        err = sqlite3_errmsg(db)
        
        if let err = err {
            if let erro = String(validatingUTF8: err) {
                return erro
            }
        }
        
        return ""
    }
    
    open static func execSqlWithTx(_ sql: String) {
        execSql("BEGIN EXCLUSIVE TRANSACTION")
        
        execSql(sql)
        
        execSql("COMMIT TRANSACTION")
    }
    
    open static func execSqlWithTxOld(_ script: String) -> OpaquePointer? {
        var stmt : OpaquePointer? = nil
        
        let sql = script.UTF8String
        
        let db = getDb()
        
        sqlite3_exec(db, "BEGIN EXCLUSIVE TRANSACTION", nil, nil, nil)
        
        // prepare
        if sqlite3_prepare_v2(db, sql, -1, &stmt, nil) != SQLITE_OK {
            sqlite3_finalize(stmt)
            log("database prepare error: \(sqlite3_errmsg(db)) - \(script)")
            
            return nil
        }
        
        // execute
        if sqlite3_exec(db, sql, nil, nil, nil) == SQLITE_OK {
            if sqlite3_exec(db, "COMMIT TRANSACTION", nil, nil, nil) != SQLITE_OK {
                log("Sql Commit Error: \(sqlite3_errmsg(db))")
            }
            
            return stmt!
            
        } else {
            sqlite3_finalize(stmt)
            log("Sql Execute Error: \(sqlite3_errmsg(db))\n[\(script)]")
            
            return nil
        }
    }
    
    static open func execSql(_ sql: String, args: [Any] = []) -> Int {
        let db = getDb()
        
        return execSql(db, sql: sql, args: args)
    }
    
    static open func execSql(_ sqls: [String]) {
        let db = getDb()
        
        for sql in sqls {
            log(sql)
            execSql(db, sql: sql)
        }
    }
    
    // Executa o SQL
    static open func execSql(_ db: OpaquePointer, sql: String, args: [Any] = []) -> Int {
        var result:CInt = 0
        
        if(DatabaseHelper.LOG_BIND_ON && args.count > 0) {
            log("\n-------------")
        }
        
        SQLUtils.log("\(sql)")

        // Statement
        guard let stmt = getStmt(sql, args: args) else {
            return 0
        }
        
        // Step
        result = try sqlite3_step(stmt)
        
        if result != SQLITE_OK && result != SQLITE_DONE {
            sqlite3_finalize(stmt)
            let msg = "Erro ao executar SQL\n\(sql)\nError: \(getLastSqlError())"
            log(msg)
            return -1
        }

        // Se for insert recupera o id
        if sql.uppercased().hasPrefix("INSERT") {
            // http://www.sqlite.org/c3ref/last_insert_rowid.html
            let rid = sqlite3_last_insert_rowid(db)
            result = CInt(rid)
        } else {
            // SQL TODO
            // https://www.sqlite.org/c3ref/changes.html
            result = 1
        }
        
        // Fecha o statement
        sqlite3_finalize(stmt)
        
        return Int(result)
    }
    
    //MARK: - Count
    
    static open func count(_ cls:AnyClass, query:String = "", args: [Any] = []) -> Int {
        var list:[T] = []

        if(DatabaseHelper.LOG_BIND_ON && args.count > 0) {
            log("\n-------------")
        }

        var sql = "select count(*) from " + SQLUtils.toSQLTableName(cls)

        if(StringUtils.isNotEmpty(query)) {
            sql += " where " + query
        }
        
        let c = count(sql, args: args)
        return c

    }
    
    static open func count(_ sql:String = "", args: [Any] = []) -> Int {
        SQLUtils.log(sql)
        
        let db = getDb()
        
        guard let stmt = getStmt(sql, args: args) else {
            return 0
        }

        let b = nextRow(stmt)
        
        var count = 0
        
        if(b) {
            count = getInt(stmt, index: Int32(0))
        }
        
        closeStatement(stmt)
        
        return count
    }
    
    // MARK: - Find
    
    static open func query(_ cls:AnyClass, sql:String, args: [Any] = []) -> [T] {
        var list:[T] = []

        if(DatabaseHelper.LOG_BIND_ON && args.count > 0) {
            log("\n-------------")
        }
        
        SQLUtils.log(sql)
        
        let db = getDb()
        
        guard let stmt = getStmt(sql, args: args) else {
            return list
        }

        
        let fields = ReflectionUtils.getFields(cls)
        
        while (nextRow(stmt)) {
            
            let entity = newEntity(cls)
            
            var idx = 0;
            for f in fields {
                var value : Any?
                
                if StringUtils.equals(f.name, withString: "id") {
                    entity.id = getInt(stmt, index: Int32(idx))
                }
                
                if f.isTypeInt() {
                    value = getInt(stmt, index: Int32(idx))
                    
                } else if (f.isType(Float.self)) {
                    value = getFloat(stmt, index: Int32(idx))
                } else if (f.isType(Double.self)) {
                    value = getDouble(stmt, index: Int32(idx))
                    
                } else if (f.isType(String.self)) {
                    value = getString(stmt, index: Int32(idx))
                    
                } else if (f.isType(Date.self) || f.isType(NSDate.self)) {
                    value = getString(stmt, index: Int32(idx))
                } else if (f.isType(Bool.self)) {
                    let i = getInt(stmt, index: Int32(idx))
                    value = i == 1 ? true : false
                } else {
                    value = getString(stmt, index: Int32(idx))
                    SQLUtils.logError("Field type not mapped: \(f). Getting default value as String: \(value)")
                }
                
                guard let newValue = value else {
                    idx += 1
                    continue
                }
                
                ReflectionUtils.setFieldValue(obj: entity, fieldName: f.name, value: newValue)
                
                idx += 1
            }
            
            list.append(entity as! T)
        }
        
        closeStatement(stmt)
        
        return list
    }
    
    /**
     *  Ex: select id,msg,data from mensagem, Retorna uma lista com todas as mensagens. Cada mensagem � um array de String[] de 3 posicoes
     *
     *  - parameter sql: strings sql.
     *  - parameter args: argumentos strings sql.
     *
     */
    static open func queryList(sql:String, args: [Any] = []) -> [[String]]{
        
        var list:[[String]] = [[]]
        //Remove na 0 pois iniciou com valor zerado
        list.remove(at: 0)
        
        SQLUtils.log(sql)
        
        let db = getDb()
        
        guard let stmt = getStmt(sql, args: args) else {
            return list
        }
        
        while (nextRow(stmt)) {
            
            var arrayString:[String] = []
            
            let size =  sqlite3_column_count(stmt)
            
            for i in 0 ... size {
                
                let s = getString(stmt, index: Int32(i))
                arrayString.append(s)
            }
            list.append(arrayString)
        }
        closeStatement(stmt)
        
        return list
    }
    
    /**
     *  Ex: select id from mensagem
     *  Resultado: Vai trazer um array de String[] com todos os resultados.
     *
     *  - parameter sql: strings sql.
     *  - parameter args: argumentos strings sql.
     *
     */
    static open func queryFields(sql:String, args: [Any] = []) -> [String]{
        let list = queryList(sql: sql, args: args)
        
        if(list.count == 0){
            return []
        }
        
        var array:[String] = []
        for l in list{
            array.append(l[0])
        }
        
        return array
    }
    
    /**
     *  Ex: select count(id) from mensagem
     *  Resultado: Vai trazer uma String com o �nico resultado.
     *
     *  - parameter sql: strings sql.
     *  - parameter args: argumentos strings sql.
     *
     */
    static open func queryField(sql:String, args: [Any] = []) -> String{
        let list = queryList(sql: sql, args: args)
        
        if(list.count == 0){
            return ""
        }
        var array:[String] = list[0]
        
        if(array != nil && array.count > 0){
            let s: String = array[0]
            return s
        }
        
        return ""
    }
    
    static func newEntity(_ cls: AnyClass) -> Entity {
        let clz: NSObject.Type = cls as! NSObject.Type
        let entity = clz.init() as! T
        return entity;
    }
    
    static open func findAll(_ cls: AnyClass) -> [T] {
        return findAll(cls, query:"", args: [], orderBy: nil, groupBy: nil)
    }
    
    static open func findAllOrderBy(_ cls:AnyClass, orderBy: String) -> [T] {
        return findAll(cls, query: nil, args: [], orderBy: orderBy, groupBy: nil)
    }
    
    static open func findAllGroupBy(_ cls:AnyClass, groupBy: String) -> [T] {
        return findAll(cls, query: nil, args: [], orderBy: nil, groupBy: groupBy)
    }
    
    static open func findAll(_ cls: AnyClass, query: String?, args: [Any] = [], orderBy: String? = nil, groupBy: String? = nil) -> [T] {
        
        var list : [T] = []
        
        var sql = "select * from \(SQLUtils.toSQLTableName(cls))"
        
        if let q = query {
            if (StringUtils.isNotEmpty(q)) {
                sql += " where \(q)"
            }
        }
        
        if let g = groupBy {
            if(StringUtils.isNotEmpty(g)){
                sql += " group by \(g)"
            }
        }
        
        if let o = orderBy {
            if (StringUtils.isNotEmpty(o)) {
                sql += " order by LOWER(\(o))"
            }
        }
        
        list = self.query(cls, sql: sql, args: args)

        return list
    }

    static open func findById(_ cls: AnyClass, id: Int) -> T? {
        let list = findAll(cls, query:"id=?",args: [id])
        if(list.count > 0) {
            return list[0]
        }
        return nil
    }
    
    static open func findAllById(_ cls: AnyClass, id: Int) -> [T]? {
        let list = findAll(cls, query:"id=?",args: [id])
        if(list.count > 0) {
            return list
        }
        return nil
    }
    
    // MARK: - Bind & SQL Type
    
    /**
     https://www.sqlite.org/datatype3.html
     **/
    static open func getSqlType(_ field:Field) -> String {
        if(field.isTypeEntity()){
            return "integer"
        }
        else if(field.isType(Int.self)) {
            return "integer"
        } else if(field.isType(Bool.self)) {
            return "integer"
        }  else if(field.isType(String.self)) {
            return "text"
        } else if(field.isType(NSDate.self) || field.isType(Date.self)) {
            return "text"
        } else if(field.isType(Double.self) || field.isType(Float.self)) {
            return "real"
        } else if(field.isType(Float.self)){
            return "text"
        }
        return "text"
    }
    
    // Faz o bind dos parametros (?,?,?) de um SQL
    static open func bindargs(_ stmt:OpaquePointer, args: [Any]) {
        let size = args.count
        
        if (size > 0) {
            for i in 1...size {
                let value : Any = args[i-1]
                
                let t = type(of:value)

                if(t == Int.self) {
                    let integer = value as! Int
                    bindInt(stmt, atIndex: i, withInteger: integer)
                } else if(t == String.self || t == Optional<String>.self) {
                    let text = value as! String
                    bindText(stmt, atIndex: i, withString: text)
                } else if(t == Bool.self) {
                    let b = value as! Bool
                    bindInt(stmt, atIndex: i, withInteger: b ? 1 : 0)
                } else if(t == Double.self) {
                    let d = value as! Double
                    bindDouble(stmt, atIndex: i, withDouble: d)
                } else {
                    log("   >> Tipo nao encontrado.")
                }
            }
        }
    }
    
    static func bindInt(_ stmt: OpaquePointer, atIndex index: Int, withInteger value: Int) {
        if(DatabaseHelper.LOG_BIND_ON) {
            log("   > bind int [\(index)]: \(value)")
        }
        sqlite3_bind_int(stmt, Int32(index), Int32(value))
    }
    
    static func bindDouble(_ stmt: OpaquePointer, atIndex index: Int, withDouble value: Double) {
        if(DatabaseHelper.LOG_BIND_ON) {
            log("   > bind double [\(index)]: \(value)")
        }
        sqlite3_bind_double(stmt, Int32(index), value)
    }
    
    static func bindText(_ stmt: OpaquePointer, atIndex index: Int, withString value: String) {
        if(DatabaseHelper.LOG_BIND_ON) {
            log("   > bind string [\(index)]: \(value)")
        }
        sqlite3_bind_text(stmt, Int32(index), value.UTF8String, -1, nil)
    }
    
    //MARK: - Get Information
    
    // Lê uma coluna do tipo Int
    static func getInt(_ stmt:OpaquePointer, index:CInt) -> Int {
        let val = sqlite3_column_int(stmt, index)
        return Int(val)
    }
    
    // Lê uma coluna do tipo Double
    static func getDouble(_ stmt:OpaquePointer, index:CInt) -> Double {
        let val = sqlite3_column_double(stmt, index)
        return Double(val)
    }
    
    // Lê uma coluna do tipo Float
    static func getFloat(_ stmt:OpaquePointer, index:CInt) -> Float {
        let val = sqlite3_column_double(stmt, index)
        return Float(val)
    }
    
    // Lê uma coluna do tipo String
    static func getString(_ stmt:OpaquePointer, index:CInt) -> String {
        let s = sqlite3_column_text(stmt, index)
        if let s = s {
            return String(cString: s)
        }
        return ""
    }
    
    //MARK: - Utils
    static open func closeStatement(_ stmt:OpaquePointer) {
        // Fecha o statement
        sqlite3_finalize(stmt)
    }
    
    
    /**
     *  Verifica se a próxima linha da consulta existe.
     *
     *  - returns: Retorna 'true', caso exista a próxima linha da consulta, e 'false' caso contrário.
     */
    static open func nextRow(_ stmt: OpaquePointer) -> Bool {
        let result = sqlite3_step(stmt)
        let next = result == SQLITE_ROW
        return next
    }
    
    // MARK: - Delete
    
    // Deleta uma entity pelo id
    static open func delete(_ entity: T) throws -> Int {

        let id = entity.getId()
        
        if(id == 0 || id == -1) {
            log("id inválido: [\(id)]")
        } else {
            //Delete
            let sql = "delete from \(SQLUtils.toSQLTableName(entity.classForCoder)) where id = \(entity.getId());"
            let count = try execSql(sql)
            
            log("delete ok")
            
            return Int(count)
        }
        
        return -1
    }
    
    static open func deleteAll(_ cls: AnyClass, whereClause clause: String? = nil, args: [Any] = []) throws -> Int {
        
        let table = SQLUtils.toSQLTableName(cls)
        var sql = "delete from \(table)"
        
        if let query = clause {
            sql += " where \(query)"
        }
        
        return try execSql(sql, args: args)
    }
    
    static open func deleteById(_ cls: AnyClass, id: Int) throws -> Bool {
        _ = try deleteAll(cls, whereClause: "id = ?", args: [id])
        return true
    }
    
    // MARK: - Utils
    
    /**
     * Retorna a lista de ids no formato id1,id2,id3
     * @param ids
     * @return
     */
    static open func toSQLIn(_ ids: [Int]) -> String {
        var sb : String = ""
        var first = true
        for id in ids {
            if(!first) {
                sb.append(",")
            }
            sb.append(String(id))
            first = false
        }
        return sb
    }

    static open func toSQLTableName(_ cls: AnyClass) -> String {
        let sql = getSqlMapping(cls)

        let tableName = sql.tableName

        if(tableName.isEmpty) {
            // Nome padrao
            let name = ClassUtils.getClassName(cls: cls)
            return toSQLName(name)
        } else {
            return tableName
        }
    }

    static open func toSQLName(_ s: String) -> String {
        
        if(StringUtils.equals(s, withString: "id")) {
            return "id"
        }
        
        var count = 0;
        var nomePattern = ""
        let array = Array(s.characters)
        for character in array {
            let str = String(character)
            if str.lowercased() != str && count > 0 {
                nomePattern.append("_\(str.lowercased())")
            } else {
                nomePattern.append(str.lowercased())
            }
            count+=1
        }
        return nomePattern
    }

    static open func toSQLColumnName(_ cls: AnyClass,fieldName: String) -> String {
        let sql = getSqlMapping(cls)

        let f = sql.fields[fieldName]
        if let f = f {
            // Existe mapeamento
            return f;
        }
        
        if(StringUtils.equals(fieldName, withString: "id")) {
            return "id"
        }
        
        var count = 0;
        var nomePattern = ""
        let array = Array(fieldName.characters)
        for character in array {
            let str = String(character)
            if str.lowercased() != str && count > 0 {
                nomePattern.append("_\(str.lowercased())")
            } else {
                nomePattern.append(str.lowercased())
            }
            count+=1
        }
        return nomePattern
    }
    
    // MARK: - Save
    // Salva um novo registro ou atualiza se já existe id
    static open func save(_ entity: T) throws -> Int {
        
        let id = entity.getId()
        
        let idIsNull = id == 0 || id == -1
        
        let fields = ReflectionUtils.getFields(entity.classForCoder)
        
        let cls: AnyClass = entity.classForCoder
        
        let db = findById(cls, id: id)
        let exists = db != nil
        
        var args : [Any] = []
        
        // params
        for f in fields {
            if idIsNull {
                if(StringUtils.equalsIgnoreCase(f.name, withString: "id")) {
                    continue
                }
            }
            
            
            var value = ReflectionUtils.getFieldValue(obj: entity, fieldName: f.name)!
            
            if StringUtils.equals(f.name, withString: "id") {
                value = id
            }
            
            if(value != nil) {
                args.append(value)
            } else {
                SQLUtils.logError("save error \(ClassUtils.getClassName(entity)) field \(f.name) not mapped")
                value = ""
            }
        }
        
        if(!exists) {
            // Insert
            var sql = "insert or replace into \(SQLUtils.toSQLTableName(entity.classForCoder)) ("
            
            // (coluna_a, coluna_b, coluna_c)
            
            var first = true
            
            for f in fields {
                
                if idIsNull {
                    if("id".equalsIgnoreCase(string: f.name)) {
                        continue
                    }
                }
                
                
                if(!first) {
                    sql.append(",");
                }
                
                let fieldName = SQLUtils.toSQLColumnName(cls, fieldName: f.name)
                sql.append(fieldName)
                
                first = false
            }
            
            // VALUES
            sql.append(") VALUES (")
            
            first = true
            
            for f in fields {
                if idIsNull {
                    if("id".equalsIgnoreCase(string: f.name)) {
                        continue
                    }
                }
                
                //
                if(!first) {
                    sql.append(",");
                }
                
                sql.append("?")
                
                first = false
            }
            
            sql.append(");")
            
            
            // exec insert
            let cid = try execSql(sql, args:args)
            let id = Int(cid)
            
            entity.id = id
            
            return id
            
        } else {
            let id = entity.getId()
            
            // Update
            var sql = "update \(SQLUtils.toSQLTableName(entity.classForCoder)) set "
            
            var first = true
            
            for f in fields {
                
                if idIsNull {
                    if("id".equalsIgnoreCase(string: f.name)) {
                        continue
                    }
                }
                
                
                if(!first) {
                    sql.append(",");
                }
                
                let value = ReflectionUtils.getFieldValue(obj: entity, fieldName: f.name);
                
                let fieldName = SQLUtils.toSQLColumnName(cls, fieldName: f.name)
                sql.append(fieldName)
                sql.append(" = ?")
                first = false
            }
            
            sql.append(" where id = \(id)" )
            
            _ = try execSql(sql, args: args)
            
            return id
        }
    }
    
    static open func saveInTx(_ cls:AnyClass, list:[T]) {
        
        var sqlStr = ""
        
        if let entity : T = list.first {
            let fields = ReflectionUtils.getFields(entity.classForCoder)
            sqlStr = "insert or replace into \(SQLUtils.toSQLTableName(entity.classForCoder)) ("
            var first = true
            for f in fields {
                if StringUtils.equalsIgnoreCase(f.name, withString: "id") {
                    continue
                }
                
                if !first {
                    sqlStr.append(",");
                }
                
                sqlStr.append(SQLUtils.toSQLColumnName(cls, fieldName: f.name))
                
                first = false
            }
            sqlStr.append(") VALUES ")
            
            for (i,e) in list.enumerated() {
                
                var sql = "("
                
                first = true
                for f in fields {
                    if StringUtils.equalsIgnoreCase(f.name, withString: "id") {
                        continue
                    }
                    if(!first) {
                        sql.append(",");
                    }
                    
                    let value = ReflectionUtils.getFieldValue(obj: e, fieldName: f.name)
                    
                    if let value = value {
                        
                        let t = type(of:value)
                        
                        if(t == Int.self) {
                            let i = value as! Int
                            sql.append("\(i)")
                        } else if(t == Bool.self) {
                            let b = value as! Bool
                            sql.append("\(b ? 1 : 0)")
                        } else if(t == Double.self) {
                            let d = value as! Double
                            sql.append("\(d)")
                        } else {
                            do {
                                let s = try value as! String
                                sql.append("'\(s)'")
                            } catch let error as NSError {
                                logError("\n\n*********** SQL ERROR: *********** ")
                                LogUtils.log("ERRO ao converter [\(f.name)] para String value \(value) ")
                                LogUtils.log(error.localizedDescription)
                                logError("*********************\n")
                                sql.append("''")
                            }
                        }
                        
                    } else {
                        sql.append("''")
                    }
                    
                    first = false
                }
                
                sql.append(")")
                sql.append(i != list.count - 1 ? "," : "")
                sqlStr.append("\(sql)")
            }
        }
        
        if sqlStr.isNotEmpty {
            _ = execSqlWithTx(sqlStr)
        }
    }
    
    // MARK: - Logs
    
    static open func printTable(_ cls: AnyClass) {
        LogUtils.log("\n### start print table \(cls) ###")
        let list: [T] = findAll(cls)
        for entity in list {
            // TODO json
            let json = String(describing: entity)
            LogUtils.log("id [\(entity.getId())] : \(json)")
        }
        LogUtils.log("### end print table \(cls) ###")
    }
    
    static open func logError(_ s: String) {
        LogUtils.log("SQL_ERROR",message: s)
    }
    
    static open func log(_ s: String) {
        if(DatabaseHelper.LOG_ON) {
            LogUtils.log("SQL",message: s)
        }
    }
    
    // MARK: - Utils
    
    static open func isTableExists(_ tableName: String) -> Bool {
        let count = self.count("select DISTINCT tbl_name from sqlite_master where tbl_name = ?", args: [tableName])
        return count > 0
    }
    
    static func getSqlLimit (_ page: Int, _ maxRows: Int) -> String {
        let offset = page * maxRows
        return "\(offset),\(maxRows)"
    }
    
    static open func getIds(_ list: [Entity]) -> String {
        var ids = "("
        
        for i in 0...list.count - 1 {
            if i > 0 {
                ids = ids + ","
            }
            
            let e = list[i]
            
            ids = "\(ids)\(e.getId())"
            
        }
        
        ids = "\(ids))"
        
        return ids
    }
}
