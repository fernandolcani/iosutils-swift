//
//  EntityMapping.swift
//  Pods
//
//  Created by Ricardo Lecheta on 17/10/16.
//
//

import Foundation

open class EntityMapping : NSObject {
    let cls:AnyClass
    
    var tableName: String = ""
    
    var fields : [String: String] = [:]
    
    var ignore : [String: String] = [:]
    
    init(cls: AnyClass) {
        self.cls = cls;
    }
    
    open func table(_ tableName: String) {
        self.tableName = tableName
    }
    
    open func column(_ fieldName: String, toColumn: String) {
        fields[fieldName] = toColumn;
    }
    
    open func ignore(_ fieldName: String) {
        ignore[fieldName] = fieldName;
    }
    
    func getTableName() -> String {
        return tableName
    }
    
    func getColumnName(_ fieldName: String) -> String {
        let c = fields[fieldName]
        if let columnName = c {
            return columnName
        }
        return ""
    }
    
    func isIgnore(_ fieldName: String) -> Bool {
        if(ignore[fieldName] != nil) {
            return true
        }
        return false
    }
    
    open override var description: String {
        return "table \(tableName), fields \(fields)"
    }
}
