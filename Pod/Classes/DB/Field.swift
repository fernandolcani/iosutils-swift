//
//  File.swift
//  HelloWorldReflection
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import Foundation

public class Field : NSObject {
    
    public let name : String
    public let type : Any.Type
    
    init(name: String, type: Any.Type) {
        if(name.equalsIgnoreCase(string: "other")) {
            //            print("boom")
        }
        self.name = name
        self.type = type
    }
    
    static func getTypeAsString(type:Any.Type, removeOptional : Bool) -> String {
        var typeS = String(describing: type)
        if(!removeOptional) {
            return typeS
        }
        typeS = typeS.replace(oldString: "Optional<", withString: "")
        typeS = typeS.replace(oldString: ">", withString: "")
        return typeS;
    }
    
    func getTypeAsString(removeOptional : Bool) -> String {
        return Field.getTypeAsString(type: self.type, removeOptional: removeOptional)
    }
    
    func getTypeAsString() -> String {
        return getTypeAsString(removeOptional: true);
    }
    
    func isOptional() -> Bool {
        let typeS = getTypeAsString(removeOptional: false)
        let b = typeS.contains("Optional")
        return b
    }
    
    func isTypeEntity() -> Bool {
        if(name.equalsIgnoreCase(string: "other")) {
            //            print("boom")
        }
        //        print(type)
        let s = getTypeAsString()
        return s.equalsIgnoreCase(string: "Int")
    }
    
    func isTypeInt() -> Bool {
        //f.type.equalsIgnoreCase(string: "Int") {
        return getTypeAsString().equalsIgnoreCase(string: "Int")
    }
    
    public func isType(_ type:String) -> Bool {
        let typeS = getTypeAsString();
        let b = typeS.equalsIgnoreCase(string: type)
        if(b) {
            return b
        }
        
        //b = typeS.contains(<#T##other: String##String#>)
        
        return b
    }
    
    public func isType(_ type:Any.Type) -> Bool {
        var b = self.type == type
        if(b) {
            return b
        }
        let type1 = getTypeAsString();
        let type2 = Field.getTypeAsString(type: type, removeOptional: false)
        b = type1 == type2;
        return b
    }
    
    override public var description: String {
        return "\(name):\(type)"
    }
}
