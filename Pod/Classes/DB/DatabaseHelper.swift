//
//  DatabaseHelper.swift
//  SugarIOS
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit

import sqlite3

public protocol DatabaseDelegate {
    
    func getDatabaseFile() throws -> String
    
    func getDatabaseVersion() throws -> Int
    
    func getDomainClasses() throws -> [AnyClass]
    
    func onCreate()
    
    func onUpgrade(_ oldVersion: Int, newVersion: Int)
}

open class DatabaseHelper : NSObject, DatabaseDelegate {
    
    // Cache de EntityMapping, chave é nome da classe.
    open var mapEntityMapping: [String: EntityMapping] = [:]
    
    // singleton
    open static var instance: DatabaseHelper?
    
    open static var LOG_ON = false
    
    open static var LOG_BIND_ON = false

    ///Equivalente do Swift para o 'sqlite3 *db'
    open var db: OpaquePointer? = nil;

    //MARK: - Init
    
    override public init() {
        super.init()

        DatabaseHelper.instance = self;

        start()
    }

    open func getDb() -> OpaquePointer {
        return db!
    }

    open func start() {
        var dbFile : String!
        do {
            dbFile = try getDatabaseFile()
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
        }
        
        let path = FileUtils.getFilePathAtDocuments(dbFile)

        let exists = FileUtils.exists(atPath: path)
        
        log("Database: \(path)")

        db = open(path)
        
        checkVersion(exists)
    }
    
    func checkVersion(_ exists: Bool) {
        do {
            let currentVersion = try getDatabaseVersion()
            
            if exists {
                
                let oldVersion = Prefs.getInt("db.version");
                
                if(oldVersion != currentVersion) {
                    SQLUtils.log("DatabaseHelper.onUpgrade (oldVersion: \(oldVersion), newVersion: \(currentVersion))")
                    
                    onUpgrade(oldVersion, newVersion: currentVersion)
                    
                    Prefs.setInt(currentVersion, forKey: "db.version")
                    
                    SQLUtils.log("DatabaseHelper.onUpgrade OK, newVersion: \(currentVersion)")
                } else {
                    SQLUtils.log("Database version [\(currentVersion)] OK.")
                    
                    if(try isAutoUpdate()) {
                        try autoUpdate()
                    }
                }
            } else {
                SQLUtils.log("DatabaseHelper.onCreate()")
                
                onCreate()

                Prefs.setInt(currentVersion, forKey: "db.version")
            }
        }
        catch {
            SQLUtils.logError("checkVersion ERROR: \(error)" )
            SQLUtils.logError(ExceptionUtils.getDBExceptionMessage(error))
        }
    }

    open func autoUpdate() throws {
        
        let domainClasses = try getDomainClasses()
        
        for cls in domainClasses {
            let tableName = SQLUtils.toSQLTableName(cls)
            
            let exists = SQLUtils.isTableExists(tableName)
            
            if(!exists) {
                SQLUtils.log("DatabaseHelper.autoUpdate, creating table \(tableName)...")
                SQLUtils.execSql(SQLUtils.getSQLCreateTable(cls))
            }
        }
    }
    
    open func onCreate() {
        SQLUtils.createDatabase()
    }
    
    open func onUpgrade(_ oldVersion: Int, newVersion: Int) {
    }

    //MARK: - Helpers
    
    // Abre o banco de dados
    open func open(_ dbFilePath: String) -> OpaquePointer? {

        var db : OpaquePointer? = nil

        let cPath = dbFilePath.UTF8String

        let result = sqlite3_open(cPath, &db)

        if(result == SQLITE_CANTOPEN) {
            log("Não foi possível abrir o banco de dados SQLite")
            return nil
        }

        return db!
    }

    // Fecha o banco de dados
    open func close() {
        sqlite3_close(db)
    }

    //MARK: - Log
    
    open func log(_ s:String) {
        SQLUtils.log(s)
    }
    
    //MARK: - Database Delegate
    
    open func getDatabaseFile() throws -> String {
        throw SQLException.notImplemented(message: "getDatabaseFile()")
    }
    
    open func getDatabaseVersion() throws -> Int {
        throw SQLException.notImplemented(message: "getDatabaseVersion()")
    }
    
    open func getDomainClasses() throws -> [AnyClass] {
        throw SQLException.notImplemented(message: "getDomainClasses()")
    }
    
    open func isAutoUpdate() throws -> Bool {
        throw SQLException.notImplemented(message: "isAutoUpdate()")
    }
}
