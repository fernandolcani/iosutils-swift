//
//  ReflectionUtils.swift
//  SugarIOS
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit

/**
 
 http://kuczborski.com/2014/07/29/any-vs-anyobject/
 
 AnyObject can represent an instance of any class type.
 Any can represent an instance of any type at all, apart from function types.
 
 AnyObject is Swift's representation of Objective-C's id type.
 It's more general than NSObject (i.e. every NSObject is an AnyObject, but not every AnyObject is a NSObject).
 
 **/
public class ReflectionUtils: NSObject {
    
    static public func setFieldValue(obj: AnyObject, fieldName: String, value: Any) {
        obj.setValue(value, forKey: fieldName)
    }
    
    static public func getFieldValue(obj: NSObject, fieldName: String) -> Any? {

        let mirror = Mirror(reflecting: obj)
        
        guard let properties = AnyBidirectionalCollection(mirror.children) else {
            return "" as NSObject
        }
        
        for property in properties {
            if(property.label == fieldName) {
                //property é tipo Mirror.Child
                // https://developer.apple.com/reference/swift/mirror/child
                // typealias Child = (label: String?, value: Any)
                
                // http://blog.krzyzanowskim.com/2015/11/19/swift-reflection-about-food/
                
                return property.value
            }
        }
        
        return "" as NSObject
    }
    
    static public func getField(_ cls: AnyClass, fieldName: String) -> Field {
    
        let fields = getFields(cls)
        
        for f in fields {
            if (f.name.equalsIgnoreCase(string: fieldName)) {
                return f;
            }
        }

        return fields[0]
    }
    
    static public func getFields(_ cls: AnyClass) -> [Field] {
        var fields : [Field] = []
        // gambi pois nao pega atributo da superclasse
        if ClassUtils.isEntity(cls) {
            let f = Field(name:"id",type:Int.self)
            fields.append(f)
        }
        let entityMapping = SQLUtils.getSqlMapping(cls)
        let clz: NSObject.Type = cls as! NSObject.Type
        let tipo = clz.init()
        let mirror = Mirror(reflecting: tipo)
        if let properties = AnyBidirectionalCollection(mirror.children) {
            for property in properties {
                if let name = property.label {
                    let ignore = entityMapping.ignore[name]
                    if ignore != nil {
                        // Existe mapeamento
                        continue
                    }
                    /**
                     type(of:)
                     https://developer.apple.com/library/content/releasenotes/DeveloperTools/RN-Xcode/Introduction.html
                    **/
                    let nativeType = type(of: property.value)
                    let t: Any.Type = type(of: property.value)
                    let f = Field(name: name, type: t)
                    fields.append(f)
                }
            }
        }

        return fields
    }

    open static func getType(_ obj: Any) -> Any.Type {
        let t = type(of:obj)
        return t
    }
    
}
