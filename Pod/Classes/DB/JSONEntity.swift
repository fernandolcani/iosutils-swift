//
//  Entity.swift
//  SugarIOS
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit
import ObjectMapper

open class JSONEntity : Entity, Mappable {
    
    // MARK: Constructor
    override public init() {
        
    }
    
    public required init?(map: Map) {
        
    }
    
    
    // MARK: JSON
    open func mapping(map: Map) {
    }
    
    
    
    //MARK: - Description
    
    override open var description: String {
        let json = toJSONString(prettyPrint: true)
        if let jsonString = json {
            return jsonString
        }
        return ""
    }
}
