//
//  JSONUtils.swift
//  Pods
//
//  Created by Ricardo Lecheta on 10/12/16.
//
//

import Foundation
import ObjectMapper

/**
 
 Classe de JSON simples que encapsula a JSONSerialization.
 
 **/
public class JSON {
    
    open class func toDictionary(_ data: Data) throws -> [String: AnyObject] {
        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: AnyObject]
        return dict
    }
    
    open class func toDictionary(_ str: String) throws -> [String: AnyObject] {
        let data = StringUtils.toData(str)
        if let data = data {
            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: AnyObject]
            return dict
        }
        return [:]
    }

    
//    open class func toDictionary(_ json: String) -> [String: AnyObject] {
//        let dict = try JSONSerialization.jsonObject(with: fileData, options: .allowFragments) as! [String: AnyObject]
//        return dict
//    }
    
    open class func toJsonDictionary(_ dict: Dictionary<String, AnyObject>) -> String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as? String
            
            if let json = string {
                
                return json;
            }
            
            return ""
        } catch let error as NSError {
            print(error)
        }
        
        return ""
    }
//
//    open class func toJsonDictionary(_ dict:NSDictionary) -> String {
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
//            
//            let string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as? String
//            
//            if let json = string {
//                
//                return json;
//            }
//            
//            return ""
//        } catch let error as NSError {
//            print(error)
//        }
//        
//        return ""
//    }

    open class func toJson(_ obj:Any) -> String {
        do {
            let valid = JSONSerialization.isValidJSONObject(obj)
            if(!valid) {
                // https://developer.apple.com/reference/foundation/jsonserialization
                print("\n##### NOT VALID JSON Object #####")
                print("@See: https://developer.apple.com/reference/foundation/jsonserialization")
                print("Object: \(obj)")
                return ""
            }
            
            let jsonData = try JSONSerialization.data(withJSONObject: obj, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as? String
            
            if let json = string {
                return json;
            }

            return ""
        } catch let error as NSError {
            print("\n ##### JSON ERROR #####")
            print(error)
        } catch {
            print("\n ##### JSON ERROR #####")
            print(error)
        }
        
        return ""
    }

    
}
