//
//  JSONObject.swift
//  Pods
//
//  Created by Ricardo Lecheta on 18/10/16.
//
//

import Foundation
import ObjectMapper

open class JSONObject : NSObject, Mappable {
    
    // MARK: Constructor
    override public init() {
        
    }
    
    public required init?(map: Map) {
        
    }

    
    // MARK: JSON
    open func mapping(map: Map) {
    }
    
    
    
    //MARK: - Description
    
    override open var description: String {
        let json = toJSONString(prettyPrint: true)
        if let jsonString = json {
            return jsonString
        }
        return ""
    }
    
}
