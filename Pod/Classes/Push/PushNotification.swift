//
//  PushNotification.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 22/07/2016.
//
//

import UIKit
import ObjectMapper

open class PushNotification : JSONObject {
    
    //MARK: - Variables
    
    static open var LOG_ON    : Bool = false
    
    fileprivate var userInfo  : [AnyHashable: Any] = [:]
    
    open var collapseKey  : String = ""
    open var from         : String = ""
    
    open var firebaseNotification : AppleNotification?
    open var appleNotification    : AppleNotification?
    
    //MARK: - Inits

    public required init?(map: Map) {
        super.init()
    }
    
    public init(_ userInfo: [AnyHashable: Any]) {
        super.init()
        
        if PushNotification.LOG_ON {
            LogUtils.log("PushNotification.parse() \(userInfo.description)")
        }
        
        collapseKey = userInfo.getStringWithKey("collapse_key")
        from = userInfo.getStringWithKey("from")
        self.userInfo = userInfo
        
        // Formato da Apple APNS
        if let apsDict = userInfo["aps"] as? [AnyHashable: Any] {
            appleNotification = AppleNotification(apsDict)
            
            if PushNotification.LOG_ON {
                LogUtils.log("PushNotification self.appleNotification: \(appleNotification)")
            }
        }

        // Formato do Firebase
        if let notificationDict = userInfo["notification"] as? [AnyHashable: Any]{
            firebaseNotification = AppleNotification(notificationDict)
            
            if PushNotification.LOG_ON {
                LogUtils.log("PushNotification self.firebaseNotification: \(firebaseNotification)")
            }
        }
    }
    
    //MARK: - Getters
    
    open func getTitle() -> String {
        
        if let appleNotification = appleNotification {
            return appleNotification.title
        }
        
        if let firebaseNotification = firebaseNotification {
            return firebaseNotification.title
        }
        
        return ""
    }
    
    open func getBody() -> String {
        
        if let appleNotification = appleNotification {
            return appleNotification.body
        }
        
        if let firebaseNotification = firebaseNotification {
            return firebaseNotification.body
        }
        
        return ""
    }
    
    open func getUserInfo() -> [AnyHashable: Any] {
        return userInfo
    }
    
    open func getBadge() -> Int {
        var contentAvailable = false
        if let appleNotif = appleNotification {
            let badge = appleNotif.badge
            return badge
        }
        return 0
    }
    
    open func isSilentPush() -> Bool {
        if let appleNotif = appleNotification {
            let b = appleNotif.content_available
            return b
        }
        return false
    }
    
    // MARK: ObjectMapper
    open override func mapping(map: Map) {
        self.userInfo               <- map["userInfo"]
        
        self.collapseKey            <- map["collapseKey"]
        self.from                   <- map["from"]
        self.firebaseNotification   <- map["firebaseNotification"]
        self.appleNotification      <- map["appleNotification"]
    }
}
