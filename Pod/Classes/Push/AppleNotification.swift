//
//  AppleNotification.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 22/07/2016.
//
//

import UIKit
import ObjectMapper

/**
 {
 "badge" : 1,
 "content-available" : "1",
 "alert" : {
 "title" : "Novo Comentário",
 "body" : "Marcio Archimedes Pissardo comentou na publicação [Gama Saúde] - Status Report"
 }
 }
 
 **/
open class AppleNotification : JSONObject {
    
    //MARK: - Variables
    
    open var userInfo   : [AnyHashable: Any] = [:]
    
    open var title        : String = ""
    open var body         : String = ""
    
    open var badge = 0
    open var content_available = false
    
    //MARK: - Inits
    public init(_ userInfo: [AnyHashable: Any]) {
        super.init()
        
        self.userInfo = userInfo;
        
        if let alertDict = userInfo["alert"] as? [AnyHashable: Any] {
            self.body = alertDict.getStringWithKey("body")
            self.title = alertDict.getStringWithKey("title")
        }
        
        badge = userInfo.getIntWithKey("badge")
        
        content_available = "1" == userInfo.getStringWithKey("content-available")
    }
    
    public required init?(map: Map) {
        super.init()
    }
    
    // MARK: ObjectMapper
    open override func mapping(map: Map) {
        self.userInfo           <- map["dictionary"]
        self.title              <- map["title"]
        self.body               <- map["body"]
        self.badge              <- map["badge"]
        self.content_available  <- map["content_available"]
        
    }
}
