//
//  PushService.swift
//  Livecom
//
//  Created by Livetouch on 19/07/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import Foundation
import iOSUtils_Swift

import UIKit

/*
 https://github.com/firebase/quickstart-ios/blob/master/messaging/FCMSwift/AppDelegate.swift
 */
class PushService: NSObject {
    
    //MARK: - Subscribe
    
    class func register(codUser: String, project: String, registrationId: String) throws {
        
        if (StringUtils.isEmpty(codUser) || StringUtils.isEmpty(project)){
            print("PushService.subscribe: Um ou mais parametros vazios, necessario preencher todos os parametros.");
            return;
        }

        if StringUtils.isEmpty(registrationId) {
            LogUtils.debug("PushService.subscribe: regId is null.")
            return
        }
        
        let wsURL =  "http://push.livetouchdev.com.br/ws/register.htm";
        var params : [String: String] = [:]
        
        // Informações do device
        let so = "iOS"
        let soVersion = "\(DeviceUtils.getVersion())"
        let deviceName = DeviceUtils.getName()
        let appVersion = AppUtils.getVersion()
        params["os"] = so
        params["os_version"] = soVersion
        params["app_version"] = appVersion
        params["device_name"] = deviceName
        
        params["projeto"] = project;
        params["registration_id"] = registrationId
        params["cod_user"] = codUser;
        params["form_name"] = "form";
        params["mode"] = "json";
        
        LogUtils.debug("PushService.subscribe:  Projeto = \(project), Usuário =  \(codUser), RegID = \(registrationId)")
        
        let http = HttpHelper()
        try http.post(wsURL, withParameters: params as [String : AnyObject])
        let json = http.getString()
        
        LogUtils.debug("PushService.subscribe: JSON = \(json)")
    }
}
