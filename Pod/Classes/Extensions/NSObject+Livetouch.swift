//
//  NSObject+Livetouch.swift
//  Pods
//
//  Created by Guilherme Politta on 17/06/16.
//
//

import Foundation
import ObjectiveC

private var queueAssociationKey: UInt8 = 0

extension NSObject {
    
    //MARK: - Queue
    
    fileprivate(set) public var queue: OperationQueue? {
        get {
            return objc_getAssociatedObject(self, &queueAssociationKey) as? OperationQueue
        }
        set(newValue) {
            objc_setAssociatedObject(self, &queueAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    //Por algum motivo, dá erro quando tenta compilar. Tem que arrumar isso
    open var maxConcurrentOperationCount : Int {
        get {
            return 5
            //if let queue = self.queue {
            //    return queue.maxConcurrentOperationCount
            //}
            //fatalError("Você deve implementar o método setupQueue: antes ober o maxConcurrentOperationCount")
        }
        set {
            print(newValue)
            //if let queue = self.queue {
            //    queue.maxConcurrentOperationCount = newValue
            //} else {
            //    fatalError("Você deve implementar o método setupQueue: antes de mudar o maxConcurrentOperationCount")
            //}
        }
    }
    
    //Por algum motivo, dá erro quando tenta compilar. Tem que arrumar isso
    public final func setupQueue() {
        self.queue = OperationQueue()
        //if let queue = self.queue {
        //    self.queue?.maxConcurrentOperationCount = 5
        //}
    }

    //MARK: - Start Tasks

    open func startTask(_ baseTask: BaseTask, withActivityIndicator activityIndicator: UIActivityIndicatorView? = nil) -> BlockOperation? {
        let taskManager = TaskManager().setTask(baseTask).setActivityIndicator(activityIndicator)
        return startTaskWithManager(taskManager)
    }

    open func startTaskOffline(_ baseTask: BaseTask, withActivityIndicator activityIndicator: UIActivityIndicatorView? = nil) -> BlockOperation? {
        let taskManager = TaskManager().setTask(baseTask).setActivityIndicator(activityIndicator).setOffline(true)
        return startTaskWithManager(taskManager)
    }

    //Ainda não implementada a lógica de paralelismo!!!!
    open func startTaskParallel(_ baseTask: BaseTask, withActivityIndicator activityIndicator: UIActivityIndicatorView? = nil) -> BlockOperation? {
        let taskManager = TaskManager().setTask(baseTask).setActivityIndicator(activityIndicator).setParallel(true)
        return startTaskWithManager(taskManager)
    }

    open func startTaskWithManager(_ taskManager: TaskManager) -> BlockOperation? {
        assert(queue != nil, "Antes de chamar uma task pela primeira vez na classe é necessário chamar o método setupQueue()")
        guard let blockOperation = taskManager.start() else {
            return nil
        }
        queue?.addOperation(blockOperation)
        return blockOperation
    }

    //MARK: - Cancel Tasks

    open func cancelTasks() {
        queue?.cancelAllOperations()
    }

    open func cancelTask(_ operation: BlockOperation?) {
        if let operation = operation {
            operation.cancel()
        }
    }

    //MARK: - Notifications

    open func registerNotification(_ notificationName: String, withSelector selector: Selector) {
        NotificationUtils.registerNotification(notificationName, withSelector: selector, fromObserver: self)
    }

    open func unregisterNotification(_ notificationName: String) {
        NotificationUtils.unregisterNotification(notificationName, fromObserver: self)
    }

    open func unregisterAllNotifications() {
        NotificationUtils.unregisterAllNotificationsFromObserver(self)
    }

    open func postNotification(_ notificationName: String, withObject object: Any? = nil) {
        NotificationUtils.postNotification(notificationName, withObject: object)
    }

    open func postNotification(_ notification: Notification) {
        NotificationUtils.postNotification(notification)
    }

    //MARK: - Conversões

    open func toJsonString() throws -> String {
        let result : AnyObject = toDictionary() as AnyObject

        let data = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
        return data.toString()
    }

    open func toDictionary() -> [String:AnyObject] {
        return NSObject.toDictionary(self)
    }

    open static func toDictionary(_ object: AnyObject) -> [String:AnyObject] {
        var dict: [String:AnyObject] = [:]
//        let otherSelf = Mirror(reflecting: object)
//
//        for child in otherSelf.children {
//            if let key = child.label {
//                if (ClassUtils.isBasicClass(child.value)) {
//                    dict[key] = (child.value as AnyObject)
//                } else {
//                    if let array = child.value as? NSArray {
//                        if (array.count > 0) {
//                            dict[key] = toArrayDescription(array) as AnyObject?
//                        }
//                    } else if let object = child.value as? NSObject {
//                        dict[key] = object.toDictionary() as AnyObject?
//                    }
//                }
//            }
//        }

        return dict
    }

    fileprivate static func toArrayDescription(_ array: NSArray) -> [AnyObject] {
        var arrayRetorno: [AnyObject] = []
        for i in array {
            if (ClassUtils.isBasicClass(i)) {
                arrayRetorno.append(i as AnyObject)
            } else if let array = i as? NSArray {
                arrayRetorno.append(array)
            } else if let object = i as? NSObject {
                arrayRetorno.append(object.toDictionary() as AnyObject)
            }
        }
        return arrayRetorno
    }

    //MARK: - Thread

    open func sleep(_ time: TimeInterval) {
        Thread.sleep(forTimeInterval: time)
    }

    open func runOnBackground(_ _block: @escaping ()->()) {
        let queue = DispatchQueue.global()
        queue.async(execute: _block)
    }

    open func runOnUIThread(_ _block: @escaping ()->()) {
        let queue = DispatchQueue.main
        queue.async(execute: _block)
    }

}
