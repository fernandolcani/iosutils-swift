//
//  NSDate+Livetouch.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 22/08/2016.
//
//

import Foundation

public extension Date {
    
    //MARK: - Format
    
    func toString(_ pattern: String = "dd/MM/yyyy") -> String {
        let string = self.string(custom: pattern)
        return string
    }
    
    //MARK: - Difference
    
    func yearsFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.year, from: date, to: self, options: []).year!
    }
    
    func yearsTo(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.year, from: self, to: date, options: []).year!
    }
    
    func monthsFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.month, from: date, to: self, options: []).month!
    }
    
    func monthsTo(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.month, from: self, to: date, options: []).month!
    }
    
    func weeksFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.weekOfYear, from: date, to: self, options: []).weekOfYear!
    }
    
    func weeksTo(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.weekOfYear, from: self, to: date, options: []).weekOfYear!
    }
    
    func daysFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.day, from: date, to: self, options: []).day!
    }
    
    func daysTo(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.day, from: self, to: date, options: []).day!
    }
    
    func hoursFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.hour, from: date, to: self, options: []).hour!
    }

    func hoursTo(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.hour, from: self, to: date, options: []).hour!
    }
    
    func minutesFrom(_ date: Date) -> Int{
        return (Calendar.current as NSCalendar).components(.minute, from: date, to: self, options: []).minute!
    }
    
    func minutesTo(_ date: Date) -> Int{
        return (Calendar.current as NSCalendar).components(.minute, from: self, to: date, options: []).minute!
    }
    
    func secondsFrom(_ date: Date) -> Int{
        return (Calendar.current as NSCalendar).components(.second, from: date, to: self, options: []).second!
    }

    func secondsTo(_ date: Date) -> Int{
        return (Calendar.current as NSCalendar).components(.second, from: self, to: date, options: []).second!
    }
    
    //MARK: Time
    func getTime() -> TimeInterval {
        return timeIntervalSince1970
    }
}
