//
//  UIImageView+Livetouch.swift
//  HelloLibIOS
//
//  Created by Livetouch on 06/07/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    // http://stackoverflow.com/questions/29814706/a-declaration-cannot-be-both-final-and-dynamic-error-in-swift-1-2
    @nonobjc open static var LOG_ON = false
    
    public func setUrl(_ surl: String,
                       placeholderImage: Image? = nil,
                       optionsInfo: KingfisherOptionsInfo? = nil,
                       progressBlock: DownloadProgressBlock? = nil,
                       completionHandler: CompletionHandler? = nil,
                       forKey: String? = nil,
                       indicator: Bool = false) /*-> RetrieveImageTask?*/ {
        
        
        if indicator {
            self.kf.indicatorType = .activity
        }
        
        if let url = URL(string: surl) {
            
            if(UIImageView.LOG_ON) {
                LogUtils.log("<UIImageView.setUrl> \(surl)")
            }
            
            if let forKey = forKey, forKey != "" {
                ImageCache.default.retrieveImage(forKey: forKey, options: nil) {
                    image, cacheType in
                    if let image = image {
                        self.image = image
                    } else {
                        
                        _ = self.kf.setImage(with: url, placeholder: placeholderImage, options: optionsInfo, progressBlock: progressBlock, completionHandler: {
                            (image, error, cacheType, imageUrl) in
                            if let image = image {
                                let image: UIImage = image
                                ImageCache.default.store(image, forKey: forKey)
                            }
                            
                            if let completionHandler = completionHandler {
                                completionHandler(image, error, cacheType, url)
                            }
                            
                        })
                        
                    }
                }
            } else {
                _ = self.kf.setImage(with: url, placeholder: placeholderImage, options: optionsInfo, progressBlock: progressBlock, completionHandler: completionHandler)
            }
        } else {
            LogUtils.log("<UIImageView.setUrl> invalid url \(surl)")
        }
    }
}

extension UIImage {
    public static func setImage(_ surl: String,
                                placeholderImage: Image? = nil,
                                optionsInfo: KingfisherOptionsInfo? = nil,
                                forKey: String? = nil,
                                progressBlock: DownloadProgressBlock? = nil,
                                completionHandler: @escaping CompletionHandler) {
        
        if let url = URL(string: surl) {
            if let forKey = forKey {
                ImageCache.default.retrieveImage(forKey: forKey, options: nil) {
                    image, cacheType in
                    if image == nil {
                        KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, url) in
                            if let image = image {
                                let image: UIImage = image
                                ImageCache.default.store(image, forKey: forKey)
                            }
                        })
                    }
                    completionHandler(image, nil, cacheType, nil)
                }
            } else {
                KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, url) in
                    completionHandler(image, error, cacheType, url)
                })
            }
        } else {
            LogUtils.log("<UIImageView.setUrl> invalid url \(surl)")
        }
    }
}
