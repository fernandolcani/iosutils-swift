//
//  UIViewController+Livetouch.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 17/06/2016.
//
//

import Foundation

private var menuContainerAssociationKey: UInt8 = 1

extension UIViewController {
    
    //MARK: - Menu
    
    fileprivate(set) open var menuContainerViewController: SideMenuContainerViewController? {
        get {
            var containerView : UIViewController? = self
            while (!(containerView is SideMenuContainerViewController) && containerView != nil) {
                containerView = containerView?.parent
                if (containerView == nil) {
                    containerView = containerView?.splitViewController
                }
            }
            return containerView as? SideMenuContainerViewController
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &menuContainerAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    //MARK: - Navigation Controller
    
    open func pushViewController(_ viewController: UIViewController, animated: Bool = true) {
        navigationController?.pushViewController(viewController, animated: animated)
    }
    
    open func popViewController(_ animated: Bool = true) {
        navigationController?.popViewController(animated: animated)
    }
    
    //MARK: - Open View Controller
    
    open func openViewController(_ viewController: UIViewController, animated: Bool = true) {
        guard let navigationController = menuContainerViewController?.getCenterViewController() as? UINavigationController else {
            return
        }
        
        navigationController.setViewControllers([viewController], animated: animated)
        menuContainerViewController?.setMenuState(.closed)
    }
    
    open func onStartEndless(task: BlockOperation?, onResetScroll: @escaping (() -> Void)) {
        // To override
    }
}
