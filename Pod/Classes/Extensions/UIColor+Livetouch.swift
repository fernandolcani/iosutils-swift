//
//  UIColor+Livetouch.swift
//  PortoSeguroCartoes
//
//  Created by livetouch PR on 10/15/15.
//  Copyright © 2015 Livetouch Brasil. All rights reserved.
//

import UIKit

public extension UIColor {
    
    // let gold = UIColor(hexString: "#ffe700ff")
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        var hex = hexString
        
        if !hex.hasPrefix("#") {
            
            hex = "#\(hex)"
        }
        
        let start = hex.index(hex.startIndex, offsetBy: 1)
        let hexColor = hex.substring(from: start)
        
        let qtde = hexColor.characters.count
        
        if qtde == 6 || qtde == 8 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                
                if(qtde == 8) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                } else {
                    r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                    g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                    b = CGFloat((hexNumber & 0x0000ff)) / 255
                    a = 1
                }
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        
        
        return nil
    }
    
    class func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
    
        UIGraphicsBeginImageContext(rect.size)
        
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    class func getColorVar(_ string: String) -> UIColor {
        if (string.characters.count > 0){
            if (string.characters.first == "-") {
                return UIColor(red: 0.980, green: 0.180, blue: 0.180, alpha: 1.0)
            } else {
                return UIColor(red: 0.564, green: 0.866, blue: 0.243, alpha: 1.0)
            }
        }
        return UIColor.gray
    }

    class func colorWithRed(_ red: Int, green: Int, blue: Int, andAlpha alpha: CGFloat) -> UIColor {
        return UIColor(red: red.toCGFloat()/255.0, green: green.toCGFloat()/255.0, blue: blue.toCGFloat()/255.0, alpha: alpha)
    }
    
    class func colorWithRGB(_ rgbValue: Int, andAlpha alpha: CGFloat) -> UIColor {
        return UIColor(red: CGFloat(rgbValue & 0xFF0000 >> 16), green: CGFloat(rgbValue & 0x00FF00 >> 8), blue: CGFloat(rgbValue & 0x0000FF >> 0), alpha: alpha)
    }

    // deprecated
    class func colorWithHex (hex:String) -> UIColor {
        //TODO:
        //        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        var cString:String = hex.trimmingCharacters(in: (NSCharacterSet.whitespaces as NSCharacterSet) as CharacterSet).uppercased()
        
        if (cString.hasPrefix("#")) {
            //TODO:
//            cString = cString.substringFromIndex(index: cString.startIndex.advancedBy(1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
