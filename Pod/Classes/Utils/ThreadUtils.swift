//
//  ThreadUtils.swift
//  Pods
//
//  Created by Ricardo Lecheta on 17/10/16.
//
//

import Foundation

open class ThreadUtils {
    
    open static func isMainThread() -> Bool {
        if let name = Thread.current.name {
            return "main" == name
        }
        return false
    }
}
