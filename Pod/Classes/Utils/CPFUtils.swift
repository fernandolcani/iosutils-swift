//
//  CPFUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 21/06/2016.
//
//

import UIKit

public class CPFUtils: NSObject {
    
    //MARK: - Mask
    
    static open func unmask(_ string: String?) -> String {
        guard let string = string else {
            return ""
        }
        
        return string.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: "/", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
    }
    
    static open func mask(_ string: String?) -> String {
        guard let string = string , string.isNotEmpty else {
            return ""
        }
        
        var cpf = unmask(string)
        
        if (cpf.length <= 3) {
            return cpf
        } else if (cpf.length <= 6) {
            return cpf.substring(to: cpf.characters.index(cpf.startIndex, offsetBy: 3))
                + "."
                + cpf.substring(from: cpf.characters.index(cpf.startIndex, offsetBy: 3))
        } else if (cpf.length <= 9) {
            return cpf.substring(to: cpf.characters.index(cpf.startIndex, offsetBy: 3))
                + "."
                + cpf.substring(from: cpf.characters.index(cpf.startIndex, offsetBy: 3)).substring(to: cpf.characters.index(cpf.startIndex, offsetBy: 3))
                + "."
                + cpf.substring(from: cpf.characters.index(cpf.startIndex, offsetBy: 6))
        } else {
            return cpf.substring(to: cpf.characters.index(cpf.startIndex, offsetBy: 3))
                + "."
                + cpf.substring(from: cpf.characters.index(cpf.startIndex, offsetBy: 3)).substring(to: cpf.characters.index(cpf.startIndex, offsetBy: 3))
                + "."
                + cpf.substring(from: cpf.characters.index(cpf.startIndex, offsetBy: 3)).substring(from: cpf.characters.index(cpf.startIndex, offsetBy: 3)).substring(to: cpf.characters.index(cpf.startIndex, offsetBy: 3))
                + "-"
                + cpf.substring(from: cpf.characters.index(cpf.startIndex, offsetBy: 9))
        }
    }
    
    static open func isMasked(_ string: String?) -> Bool {
        if let string = string {
            return string.contains(".")
        }
        return false
    }
    
    //MARK: - Validation
    
    static open func isValid(string: String?) -> Bool {
        guard let string = string , string.isNotEmpty else {
            return false
        }
        
        let cpf = CPFUtils.unmask(string)
        if (cpf.length != 11) {
            return false
        }
        
        var primeiroDigitoVerificador = ""
        var segundoDigitoVerificador = ""
        
        var j = 0
        var sum = 0
        var result = 0
        
        for weight in stride(from: 10, to: 1, by: -1) {
            let substring = cpf[cpf.characters.index(cpf.startIndex, offsetBy: j)]
            let segment = String(substring).utf8
            
            sum += weight * ("\(segment)".integerValue)
            j += 1
        }
        
        result = 11 - (sum % 11)
        if (result == 10 || result == 11) {
            primeiroDigitoVerificador = "0"
        } else {
            primeiroDigitoVerificador.append(String(describing: UnicodeScalar(result + 48)!))
        }
        
        //        let penultimoDigito = cpf.substring(with: cpf.characters.index(cpf.startIndex, offsetBy: 9)...cpf.characters.index(cpf.endIndex, offsetBy: -2))
        //        if (!primeiroDigitoVerificador.equalsIgnoreCase(penultimoDigito)) {
        //            return false
        //        }
        
        let penultimoDigito = cpf[cpf.characters.index(cpf.startIndex, offsetBy: 9)]
        //            cpf.substringWith(cpf.characters.index(cpf.startIndex, offsetBy: 9)..<cpf.characters.index(cpf.endIndex, offsetBy: -2))
        if (!primeiroDigitoVerificador.equalsIgnoreCase(string: String(penultimoDigito))) {
            return false
        }
        
        j = 0
        sum = 0
        
        for weight in stride(from: 11, to: 1, by: -1) {
            let substring = cpf[cpf.characters.index(cpf.startIndex, offsetBy: j)]
            let segment = String(substring).utf8
            
            sum += weight * ("\(segment)".integerValue)
            j += 1
        }
        
        result = 11 - (sum % 11)
        if (result == 10 || result == 11) {
            segundoDigitoVerificador = "0"
        } else {
            segundoDigitoVerificador.append(String(describing: UnicodeScalar(result + 48)!))
        }
        
        //        let ultimoDigito = cpf.substring(with: cpf.characters.index(cpf.startIndex, offsetBy: 10)...cpf.characters.index(cpf.endIndex, offsetBy: -1))
        //        let ultimoDigito = cpf.substringWith(cpf.startIndex.advancedBy(10)...cpf.endIndex.advancedBy(-1))
        let ultimoDigito = cpf[cpf.characters.index(cpf.startIndex, offsetBy: 10)]
        
        if (!segundoDigitoVerificador.equalsIgnoreCase(string: String(ultimoDigito))) {
            return false
        }
        
        return true
    }

}
