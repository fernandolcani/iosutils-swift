//
//  MessageUtils.swift
//  Pods
//
//  Created by Ricardo Lecheta on 21/10/16.
//
//

import Foundation

open class MessageUtils {
    
    static open func getDict(fileName: String) -> [String: AnyObject] {

        var fileData = try? FileUtils.getBundleFile(fileName, ofType: nil)
        if (fileData == nil) {
            fileData = try? FileUtils.getLibFile(fileName, ofType: nil)
        }
        
        do {
            guard let fileData = fileData else {
                LogUtils.log("Arquivo '\(fileName)' não se encontra no projeto.")
                return [:]
            }
            
            let dict = try JSON.toDictionary(fileData)
            
//            return json.getStringWithKey(tag)
            
            return dict
            
        } catch {
            LogUtils.log("\n###\nArquivo '\(fileName)' está formatado incorretamente.\n###\n")
            return [:]
        }
    }
}
