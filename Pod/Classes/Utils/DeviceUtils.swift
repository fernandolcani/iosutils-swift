//
//  DeviceUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 17/06/2016.
//
//

import Foundation

import AudioToolbox

open class DeviceUtils: NSObject {
    
    //MARK: - Helpers
    
    static fileprivate func getDigitsOnlyPhoneNumber(_ phoneNumber: String) -> String {
        let numberArray = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted)
        let number = numberArray.joined(separator: "")
        return number
    }
    
    //MARK: - Actions
    
    static open func call(_ phoneNumber: String) {
        let number = getDigitsOnlyPhoneNumber(phoneNumber)
        if let nsurl = URL(string: "tel://\(number)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(nsurl)
            } else {
                UIApplication.shared.openURL(nsurl)
            }
        }
    }
    
    static open func sms(_ phoneNumber: String) {
        let number = getDigitsOnlyPhoneNumber(phoneNumber)
        if let nsurl = URL(string: "sms://\(number)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(nsurl)
            } else {
                UIApplication.shared.openURL(nsurl)
            }
        }
    }
    
    static open func openSettings() {
        if let nsurl = URL(string: UIApplicationOpenSettingsURLString) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(nsurl)
            } else {
                UIApplication.shared.openURL(nsurl)
            }
        }
    }
    
    static open func vibrate() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    //MARK: - Information
    
    /**
     * Retorna o UDID para identificar o celular
     */
    static open func getUUID() -> String {
        let uuid = UUID().uuidString
        return uuid
    }
    
    static open func getPlatformCode() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let platform = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 , value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return platform
    }
    
    static open func getName() -> String {
        let model = getPlatformCode()
        let version = "\(getVersion())"
        let uniqueIdentifier = getUUID()
        var name = "\(model)_\(version)_\(uniqueIdentifier)"
        name = StringUtils.replace(name, inQuery: ",", withReplacement: ".")
        name = StringUtils.replace(name, inQuery: "-", withReplacement: "_")
        return name
    }
    
    //MARK: - Clipboard
    
    static open func copyToClipboard(_ text: String?) {
        let pasteboard = UIPasteboard.general
        pasteboard.string = text
    }
    
    //MARK: - Sizes
    
    static open func getScreenSize() -> CGSize {
        return UIScreen.main.bounds.size
    }
    
    static open func getScreenWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    static open func getScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    //MARK: - Scales
    
    static open func getScreenScale() -> CGFloat {
        return UIScreen.main.scale
    }
    
    static open func isNormalDisplay() -> Bool {
        let version = getVersionInt()
        if (version < 4) {
            return false
        }
        
        let scale = getScreenScale()
        return scale == 1.0
    }
    
    static open func isRetinaDisplay() -> Bool {
        let version = getVersionInt()
        if (version < 4) {
            return false
        }
        
        let scale = getScreenScale()
        return scale == 2.0
    }
    
    static open func isHDDisplay() -> Bool {
        let version = getVersionInt()
        if (version < 4) {
            return false
        }
        
        let scale = getScreenScale()
        return scale == 3.0
    }
    
    //MARK: - Model
    
    static open func getModel() -> String  {
        return UIDevice.current.model
    }
    
    static open func isSimulator() -> Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    static open func isIphone() -> Bool {
        let idiom = UIDevice.current.userInterfaceIdiom
        return idiom == .phone
    }
    
    static open func isIpad() -> Bool {
        let idiom = UIDevice.current.userInterfaceIdiom
        return idiom == .pad
    }
    
    static open func isIphone4() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 480.0
    }
    
    static open func isIphone5() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 568.0
    }
    
    static open func isIphone6() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 667.0
    }
    
    static open func isIphone6Plus() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 736.0
    }
    
    static open func isIphoneX() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 812.0
    }
    
    //MARK: - Version
    static open func getVersion() -> String {
        //retorna 9.3.2
        return UIDevice.current.systemVersion
    }
    
    static open func getVersionInt() -> Int {
        let soVersion = ProcessInfo().operatingSystemVersion.majorVersion
        return soVersion
    }
    
    static open func isIOS8() -> Bool {
        if #available(iOS 8.0, *) {
            return true
        }
        return false
    }
    
    static open func isIOS9() -> Bool {
        if #available(iOS 9.0, *) {
            return true
        }
        return false
    }
    
    static open func isIOS10() -> Bool {
        if #available(iOS 10.0, *) {
            return true
        }
        return false
    }
    
    static open func isIOS11() -> Bool {
        if #available(iOS 11.0, *) {
            return true
        }
        return false
    }
    
    static open func getModelName() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPodTouch5"
        case "iPod7,1":                                 return "iPodTouch6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone4"
        case "iPhone4,1":                               return "iPhone4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone5s"
        case "iPhone7,2":                               return "iPhone6"
        case "iPhone7,1":                               return "iPhone6Plus"
        case "iPhone8,1":                               return "iPhone6s"
        case "iPhone8,2":                               return "iPhone6sPlus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone7Plus"
        case "iPhone8,4":                               return "iPhoneSE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhoneX"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPadAir"
        case "iPad5,3", "iPad5,4":                      return "iPadAir 2"
        case "iPad6,11", "iPad6,12":                    return "iPad5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPadMini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPadMini2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPadMini3"
        case "iPad5,1", "iPad5,2":                      return "iPadMini4"
        case "iPad6,3", "iPad6,4":                      return "iPadPro9.7Inch"
        case "iPad6,7", "iPad6,8":                      return "iPadPro12.9Inch"
        case "iPad7,1", "iPad7,2":                      return "iPadPro12.9Inch2.Generation"
        case "iPad7,3", "iPad7,4":                      return "iPadPro10.5Inch"
        case "AppleTV5,3":                              return "AppleTV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}
