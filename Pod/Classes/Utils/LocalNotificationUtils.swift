//
//  LocalNotificationUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 22/07/2016.
//
//

import UIKit
import UserNotifications
import AVFoundation


open class LocalNotificationUtils: NSObject {
    
    //MARK: - Show
    
    /**
     Mostra uma notificação de push.
     
     - warning: Caso o título da notificação de push passada seja nil ou uma String vazia, a notificação local não será mostrada.
     - warning: A notificação tera o identifier "Notification" por padrão.
     
     - important: Este método é equivalente a chamar LocalNotificationUtils.show(notification).
     
     - parameter notification: Notificação de push a ser agendada (classe 'PushNotification')
     - parameter fireDate: Data em que a notificação será disparada. Caso este parâmetro não seja passado, o valor padrão será dia e hora atuais.
     */
    static open func showPush(_ notification: PushNotification) {
        show(notification)
    }
    
    //MARK: - Schedule
    
    /**
     Agenda uma notificação de push. Isso permite que notificações de push apareçam mesmo quando o aplicativo estiver aberto.
     
     - warning: Caso o título da notificação de push passada seja nil ou uma String vazia, a notificação local não será agendada.
     - warning: A notificação tera o identifier "Notification" por padrão.
     
     - parameter notification: Notificação de push a ser agendada (classe 'PushNotification')
     - parameter fireDate: Data em que a notificação será disparada. Caso este parâmetro não seja passado, o valor padrão será dia e hora atuais.
     */
    static open func show(_ notification: PushNotification, atDate fireDate: Date = Date(timeIntervalSinceNow: 0), identifier: String? = "Notification") {
        
        if (StringUtils.isEmpty(notification.getTitle())) {
            LogUtils.log("Local push error! Notificação inválida!")
            
            return
        }
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            
            
            let localNotification = UNMutableNotificationContent()
            localNotification.title = notification.getTitle()
            localNotification.body = notification.getBody()
            localNotification.userInfo = notification.getUserInfo()
            localNotification.badge = notification.getBadge() as NSNumber?
            localNotification.sound = UNNotificationSound.default()
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0, repeats: false)
            
            let request = UNNotificationRequest(identifier: identifier!,
                                                content: localNotification, trigger: trigger)
            
            center.add(request, withCompletionHandler: { (error) in
                if let error = error {
                    LogUtils.log("iOS 10 Notification Failure")
                }
            })
            
        } else {
            let localNotification = UILocalNotification()
            localNotification.fireDate = fireDate
            
            if #available(iOS 8.2, *) {
                localNotification.alertTitle = notification.getTitle()
            } else {
                localNotification.alertAction = notification.getTitle()
            }
            
            localNotification.alertBody = notification.getBody()
            localNotification.userInfo = notification.getUserInfo()
            localNotification.applicationIconBadgeNumber = notification.getBadge()
            localNotification.soundName = UILocalNotificationDefaultSoundName
            localNotification.hasAction = true
            
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
    }
    
    /**
     Agenda uma notificação local. Isso faz com que alertas apareçam quando a notificação for disparada.
     
     - warning: Caso o título passado seja nil ou uma String vazia, a notificação não será agendada.
     
     - parameter title: Título da notificação
     - parameter body: Corpo de mensagem da notificação. Caso este parâmetro não seja passado, o valor padrão será uma String vazia.
     - parameter userInfo: Dicionário de informações da notificação. Caso este parâmetro não seja passado, o valor padrão será nil.
     - parameter badgeNumber: Número indicador da notificação. Caso este parâmetro não seja passado, o valor padrão será 0 (zero).
     - parameter fireDate: Data em que a notificação será disparada. Caso este parâmetro não seja passado, o valor padrão será dia e hora atuais.
     */
    static open func show(_ title: String, body: String = "", andUserInfo userInfo: [AnyHashable: Any]? = nil, withBadgeCount badgeNumber: Int = 0, atDate fireDate: Date = Date(timeIntervalSinceNow: 0),isBeep : Bool, isVibration: Bool, identifier: String?) {
        
        if (StringUtils.isEmpty(title)) {
            LogUtils.log("Local notification error! Título vazio!")
            
            return
        }
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            
            let localNotification = UNMutableNotificationContent()
            localNotification.title = title
            localNotification.body = body
            if let userInfo = userInfo {
                localNotification.userInfo = userInfo
            }
            localNotification.badge = badgeNumber as NSNumber?
            
            if(isBeep){
                AudioServicesPlaySystemSound(1016)
            }
            if(isVibration){
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
            //Se timeInterval <= 0, o app crasha.
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            
            let request = UNNotificationRequest(identifier: identifier!,
                                                content: localNotification, trigger: trigger)
            
            center.add(request, withCompletionHandler: { (error) in
                if let error = error {
                    LogUtils.log("iOS 10 Notification Failure")
                }
            })
            
        } else {
            let localNotification = UILocalNotification()
            localNotification.fireDate = fireDate
            
            if #available(iOS 8.2, *) {
                localNotification.alertTitle = title
            } else {
                localNotification.alertAction = title
            }
            
            localNotification.alertBody = body
            localNotification.userInfo = userInfo
            localNotification.applicationIconBadgeNumber = badgeNumber
            localNotification.soundName = UILocalNotificationDefaultSoundName
            localNotification.hasAction = true
            
            if(isBeep){
                AudioServicesPlaySystemSound(1016)
            }
            if(isVibration){
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
            
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
    }
}
