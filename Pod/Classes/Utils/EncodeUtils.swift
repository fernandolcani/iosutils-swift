//
//  EncodeUtils.swift
//  Pods
//
//  Created by LivetouchUser on 27/10/16.
//
//

import UIKit

open class EncodeUtils: NSObject {
 
    static open func encodeToUTF8(s:String) -> String{
  
        do{
            
            let str = try String(CFURLCreateStringByAddingPercentEscapes(nil, s as CFString!, nil,"/%&=?$#+-~@<>|\\*,.()[]{}^!" as CFString!,CFStringBuiltInEncodings.UTF8.rawValue)!)
            
            return str
       
        }catch{
 
            return s
            
        }
    }
}

