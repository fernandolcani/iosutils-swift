//
//  KeyboardUtils.swift
//  Pods
//
//  Created by Livetouch BR on 6/22/16.
//
//

import UIKit

open class KeyboardUtils: NSObject {
    
    //MARK: - Show
    
    static open func show(_ view: UIKeyInput?) {
        guard let view = view as? UIView else {
            return
        }
        
        let selector = #selector(view.becomeFirstResponder)
        
        if (view.responds(to: selector)) {
            view.perform(selector)
        }
    }
    
    static open func hide(_ view: UIView?) {
        guard let view = view else {
            return
        }
        
        view.endEditing(true)
    }
    
    //MARK: - Sizes
    
    static open func getKeyboardSizeFromNotification(_ notification: Notification) -> CGSize {
//        guard let userInfo = (notification as NSNotification).userInfo else {
//            return CGSize(width: 0.0, height: 0.0)
//        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            return keyboardSize.size
        }
        return CGSize(width: 0.0, height: 0.0)
    }
    
    static open func getKeyboardWidthFromNotification(_ notification: Notification) -> CGFloat {
        let size = getKeyboardSizeFromNotification(notification)
        return size.width
    }
    
    static open func getKeyboardHeightFromNotification(_ notification: Notification) -> CGFloat {
        let size = getKeyboardSizeFromNotification(notification)
        return size.height
    }
    
}
