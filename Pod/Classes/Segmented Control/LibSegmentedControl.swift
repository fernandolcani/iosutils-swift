//
//  LibSegmentedControl.swift
//  Pods
//
//  Created by Livetouch BR on 6/23/16.
//
//

/*
 *  Esta classe foi alterada a partir da classe disponibilizada abaixo.
 *  http://www.appdesignvault.com/custom-segmented-control-swift-tutorial/
 */

import UIKit

public protocol LibSegmentedControlDelegate {
    func onCliksegmentedControl(_ control: UIControl, didChangeIndex index: Int)
}

@IBDesignable open class LibSegmentedControl: UIControl {
    
    fileprivate var labels = [UILabel]()
    var viewSegmentSelected = UIView()
    
    public var items: [String] = ["Item 1", "Item 2", "Item 3"] {
        didSet {
            setupLabels()
        }
    }
    
    public var selectedSegmentIndex : Int = 0 {
        didSet {
            displayNewSelectedIndex()
        }
    }
    
    public var delegate : LibSegmentedControlDelegate?
    
    //MARK: - Variaveis View
    /**
     *  Método para alterar cor da label do segmento selecionado(Cor padrão white).
     *
     *  - parameter UIColor: Recebe uma cor.
     *
     */
    @IBInspectable public var selectedLabelColor : UIColor = UIColor.white {
        didSet {
            setSelectedColors()
        }
    }
    
    /**
     *  Método para alterar cor da label do segmento não selecionado(Cor padrão black).
     *
     *  - parameter UIColor: Recebe uma cor.
     *
     */
    @IBInspectable public var unselectedLabelColor : UIColor = UIColor.black {
        didSet {
            setSelectedColors()
        }
    }
    
    /**
     *  Método para alterar cor do backgorund do segmento não selecionado(Cor padrão red).
     *
     *  - parameter UIColor: Recebe uma cor.
     *
     */
    @IBInspectable public var viewBackgroundColor : UIColor = UIColor.red {
        didSet{
            layer.backgroundColor = viewBackgroundColor.cgColor
        }
    }
    
    /**
     *  Método para alterar cor da borda do segmented control (Cor padrão blue).
     *
     *  - parameter UIColor: Recebe uma cor.
     *
     */
    @IBInspectable public var viewBorderColor : UIColor = UIColor.blue {
        didSet {
            layer.borderColor = viewBorderColor.cgColor
        }
    }
    
    /**
     *  Método para alterar o width borda do segmented control (Tamanho padrão 5).
     *
     *  - parameter CGFloat: Recebe um valor ex: 0.0.
     *
     */
    @IBInspectable public var viewBorderWidth : CGFloat = 5 {
        didSet {
            layer.borderWidth = viewBorderWidth
        }
    }
    
    /**
     *  Método para arredondar a borda do segmented control (Tamanho padrão 0).
     *
     *  - parameter CGFloat: Recebe um valor ex: 0.0.
     *
     */
    @IBInspectable public var viewCornerRadius : CGFloat = 0 {
        didSet {
            layer.cornerRadius = viewCornerRadius
        }
    }
    
    /**
     *  Método para alterar a fonte das labels do segmented control (Fonte padrão UIFont(name: "Helvetica", size: 14)).
     *
     *  - parameter UIFont: Recebe uma UIFont ex: UIFont(name: "Helvetica", size: 14).
     *
     */
    @IBInspectable public var labelFont : UIFont? = UIFont.systemFont(ofSize: 17.0) {
        didSet {
            setFont()
        }
    }
    
    //MARK: - Variaveis View Segment selected
    /**
     *  Método para alterar o background do segmento selecionado (Cor padrão gray).
     *
     * - parameter UIColor: Recebe uma cor.
     *
     */
    @IBInspectable public var segmentSelectedColor : UIColor = UIColor.gray {
        didSet {
            setSelectedColors()
        }
    }
    
    /**
     *  Método para arredondar a borda do segmento selecionado(Tamanho padrão 0).
     *
     *  - parameter CGFloat: Recebe um valor ex: 0.0.
     *
     */
    @IBInspectable public var segmentSelectedCornerRadius : CGFloat = 0 {
        didSet {
            viewSegmentSelected.layer.cornerRadius = segmentSelectedCornerRadius
        }
    }
    
    //MARK: - Animação da transição
    /**
     *  Método de animação na transição dos segmentos (Padrão true).
     *
     *  - parameter Bool: Recebe um valor ex: true ou false.
     *
     */
    @IBInspectable public var springWithDamping : Bool = true {
        didSet {
            displayNewSelectedIndex()
        }
    }
    
    //MARK: - Habilitar a seleção de um novo segmento
    /**
     *  Método para abilitar a seleção de um novo segmento (Padrão true).
     *
     *  - parameter Bool: Recebe um valor ex: true ou false.
     *
     */
    @IBInspectable public var isEnable : Bool = true {
        didSet {
            displayNewSelectedIndex()
        }
    }
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    //MARK: - Setups
    func setupView(){
        
        layer.cornerRadius = viewCornerRadius
        layer.borderColor = viewBorderColor.cgColor
        layer.borderWidth = viewBorderWidth
        
        backgroundColor = viewBackgroundColor
        
        setupLabels()
        
        addIndividualItemConstraints(labels, mainView: self, padding: 0)
        
        insertSubview(viewSegmentSelected, at: 0)
        
        addTarget(self, action: #selector(onClickSegControl), for: .valueChanged)
    }
    
    func setupLabels(){
        
        for label in labels {
            label.removeFromSuperview()
        }
        
        labels.removeAll(keepingCapacity: true)
        
        for index in 1...items.count {
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 40))
            label.text = items[index - 1]
            label.backgroundColor = UIColor.clear
            label.textAlignment = .center
            label.font = labelFont
            label.textColor = index == 1 ? selectedLabelColor : unselectedLabelColor
            label.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(label)
            labels.append(label)
        }
        
        addIndividualItemConstraints(labels, mainView: self, padding: 0)
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        var selectFrame = self.bounds
        let newWidth = selectFrame.width / CGFloat(items.count)
        selectFrame.size.width = newWidth
        viewSegmentSelected.frame = selectFrame
        
        viewSegmentSelected.backgroundColor = segmentSelectedColor
        viewSegmentSelected.layer.cornerRadius = segmentSelectedCornerRadius
        displayNewSelectedIndex()
        
    }
    
    //MARK: - Helpers
    func displayNewSelectedIndex(){
        
        if(isEnable){
            for (_, item) in labels.enumerated() {
                item.textColor = unselectedLabelColor
            }
            
            let label = labels[selectedSegmentIndex]
            label.textColor = selectedLabelColor
            
            var valueDamping: CGFloat = 1
            if(springWithDamping){
                valueDamping = 0.5
            }
            
            UIView.animate(withDuration: 0.5, delay: 0.00, usingSpringWithDamping: valueDamping, initialSpringVelocity: 0.8, options: [], animations: {
                
                self.viewSegmentSelected.frame = label.frame
                
                }, completion: nil)
        }
    }
    
    func addIndividualItemConstraints(_ items: [UIView], mainView: UIView, padding: CGFloat) {
        
        for (index, button) in items.enumerated() {
            
            let topConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
            
            let bottomConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
            
            var rightConstraint : NSLayoutConstraint!
            
            if index == items.count - 1 {
                
                rightConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: -padding)
                
            }else{
                
                let nextButton = items[index+1]
                rightConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: nextButton, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: -padding)
            }
            
            
            var leftConstraint : NSLayoutConstraint!
            
            if index == 0 {
                
                leftConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: padding)
                
            }else{
                
                let prevButton = items[index-1]
                leftConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: prevButton, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: padding)
                
                let firstItem = items[0]
                
                let widthConstraint = NSLayoutConstraint(item: button, attribute: .width, relatedBy: NSLayoutRelation.equal, toItem: firstItem, attribute: .width, multiplier: 1.0  , constant: 0)
                
                mainView.addConstraint(widthConstraint)
            }
            
            mainView.addConstraints([topConstraint, bottomConstraint, rightConstraint, leftConstraint])
        }
    }
    
    func setSelectedColors(){
        for item in labels {
            item.textColor = unselectedLabelColor
        }
        
        if labels.count > 0 {
            labels[0].textColor = selectedLabelColor
        }
        
        viewSegmentSelected.backgroundColor = segmentSelectedColor
    }
    
    func setFont(){
        for item in labels {
            item.font = labelFont
        }
    }
    
    //MARK: - Action
    @objc func onClickSegControl() {
        delegate?.onCliksegmentedControl(self, didChangeIndex: selectedSegmentIndex)
    }
    
    override open func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        
        let location = touch.location(in: self)
        
        var calculatedIndex : Int?
        for (index, item) in labels.enumerated() {
            if item.frame.contains(location) {
                calculatedIndex = index
            }
        }
        
        if calculatedIndex != nil {
            selectedSegmentIndex = calculatedIndex!
            sendActions(for: .valueChanged)
        }
        
        return false
    }
}
