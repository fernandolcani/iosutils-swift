#
# Be sure to run `pod lib lint iOSUtils-Swift.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "iOSUtils-Swift"
  s.version          = "1.7"
  s.summary          = "Lib de iOS da Livetouch em Swift."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                        "Lib de iOS da Livetouch em Swift para facilitar desenvolvimento de aplicativos na plataforma iOS."
                       DESC

  s.homepage         = "https://bitbucket.org/livetouchdev/iosutils-swift"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Desenvolvimento Livetouch" => "desenvolvimento@livetouch.com.br" }
  s.source           = { :git => "https://bitbucket.org/livetouchdev/iosutils-swift", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resources = 'Pod/Resources/**/*'
#  s.resource_bundles = {
#   'iOSUtils-Swift' => ['Pod/Assets/*.png']
#  }

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.public_header_files = 'Pod/Classes/Db/Obj-C/iOSUtils-Bridging-Header.h'
# s.frameworks = 'UIKit', 'MapKit'

s.dependency 'sqlite3',      '<= 3.15.0' #
s.dependency 'CryptoSwift',  '<= 0.9.0'  #https://github.com/krzyzanowskim/CryptoSwift/
s.dependency 'SwiftDate',    '<= 4.5.1'  #https://github.com/malcommac/SwiftDate/
s.dependency 'SimpleExif'
s.dependency 'Kingfisher',   '<= 4.8.0'  #https://github.com/onevcat/Kingfisher/
s.dependency 'ObjectMapper', '<= 3.2'    #https://github.com/Hearst-DD/ObjectMapper/
                             #'<= 2.1.0'

end
